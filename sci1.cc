//
//  sci1.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "sci1.h"
#include <map>
#include <string>
#include "academic.h"
using namespace std;


map<string,string> SCI1::ownerList;
map<string,int> SCI1::improvementList;

SCI1::SCI1(int improveCost,int purchaseCost,std::string name):Academic(improveCost,purchaseCost,name){
    ownerList["PHYS"] = "BANK";
    ownerList["B1"] = "BANK";
    ownerList["B2"] = "BANK";
    improvementList["PHYS"] = 0;
    improvementList["B1"] = 0;
    improvementList["B2"] = 0;
}

SCI1::~SCI1(){}