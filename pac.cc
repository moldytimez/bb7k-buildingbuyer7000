//
//  pac.cpp
//  bb7k
//
//  Created by jessica on 14/11/21.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "pac.h"
#include "gym.h"
#include <string>
using namespace std;

PAC::PAC():GYM("PAC"){}

string PAC::getOwner(){
    return owner;
}

void PAC::setOwner(string ownerPiece){
    if(owner != ownerPiece && owner != "BANK") {
        gymCount[owner]--;
    }
    owner = ownerPiece;
    gymCount[ownerPiece]++;
}


int PAC::getFees(){
    return usageFee;
}

int PAC::getFees(string ownerPiece){
    int dice1 = giveDice();
    int dice2 = giveDice();
    cout << "-------INFORMATION-------" << endl;
    cout << "Two dice will be rolled; fees to pay depend on number of gyms the owner owns" << endl;
    cout << "The owner owns " << gymCount[ownerPiece] << " gym(s)" << endl;
    cout << dice1 << " and " << dice2 << " are rolled" << endl;
    if(gymCount[ownerPiece] == 1)  usageFee = 4 * (dice1 + dice2);
    if(gymCount[ownerPiece] == 2)  usageFee = 10 * (dice1 + dice2);
    return usageFee;
}



int PAC::getPurchaseCost(){
    return purchaseCost;
}

string PAC::getName(){
    return name;
}

bool PAC::getMortgage(){
    return isMortgage;
}

void PAC::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

int PAC::getImprovementCost(){return 0;};
void PAC::changeImprovement(string act){}
int PAC::getImprovement(){return 0;}
bool PAC::checkMonopoly(bool whetherPrint){ return false;}
bool PAC::checkMonopoly(){return false;}
PAC::~PAC(){}

