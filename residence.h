//
//  residence.h
//  bb7k
//
//  Created by jessica on 14/11/20.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#ifndef __RESIDENCE_H__
#define __RESIDENCE_H__

#include <iostream>
#include <map>
#include <string>
#include "building.h"

class Residence: public Building{
protected:
    int rent;
public:
    Residence(std::string name);
    ~Residence();
};

#endif