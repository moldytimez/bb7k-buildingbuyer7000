//
//  al.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "al.h"
#include "arts1.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

AL::AL():Arts1(50,40,"AL"){
    tuitionList[0] = 2;
    tuitionList[1] = 10;
    tuitionList[2] = 30;
    tuitionList[3] = 90;
    tuitionList[4] = 160;
    tuitionList[5] = 250;
}


int AL::getPurchaseCost(){
    return purchaseCost;
}

int AL::getImprovementCost(){
    return improvementCost;
}

void AL::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["AL"] = improvement;
}


int AL::getImprovement(){
    return improvement;
}

bool AL::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["ML"] == owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        cout << "The property required to be owned is ML (owner: " << ownerList["ML"]<< ")" << endl;
    }
    return isMonopoly;
}


bool AL::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["ML"] == owner) isMonopoly = true;
    return isMonopoly;
}


int AL::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["ML"] == 0) tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int AL::getFees(string ownerPiece){return 0;}

string AL::getName(){
    return name;
}

string AL::getOwner(){
    return owner;
}

void AL::setOwner(string name){
    owner = name;
    ownerList["AL"] = name;
}


bool AL::getMortgage(){
    return isMortgage;
}

void AL::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}


AL::~AL(){}
























