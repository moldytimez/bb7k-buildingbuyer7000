//
//  ev1.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "ev1.h"
#include "env.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

EV1::EV1():ENV(150,220,"EV1"){
    tuitionList[0] = 18;
    tuitionList[1] = 90;
    tuitionList[2] = 250;
    tuitionList[3] = 700;
    tuitionList[4] = 875;
    tuitionList[5] = 1050;
}


int EV1::getPurchaseCost(){
    return purchaseCost;
}

int EV1::getImprovementCost(){
    return improvementCost;
}

void EV1::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["EV1"] = improvement;
}


int EV1::getImprovement(){
    return improvement;
}

bool EV1::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["EV2"] == owner && ownerList["EV3"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["EV2"] == owner && ownerList["EV3"] != owner)
            cout << "The property required to be owned is EV3 (owner: " << ownerList["EV3"] << ")" << endl;
        if(ownerList["EV2"] != owner && ownerList["EV3"] == owner)
            cout << "The property required to be owned is EV2 (owner: " << ownerList["EV2"] << ")" << endl;
        if(ownerList["EV2"] != owner && ownerList["EV3"] != owner){
            cout << "The property required to be owned is EV2 (owner: " << ownerList["EV2"] << ")" << endl;
            cout << "The property required to be owned is EV3 (owner: " << ownerList["EV3"] << ")" << endl;
        }
    }
    return isMonopoly;
}


bool EV1::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["EV2"] == owner && ownerList["EV3"]== owner) isMonopoly = true;
    return isMonopoly;
}


int EV1::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["EV2"] == 0 && improvementList["EV3"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int EV1::getFees(string ownerPiece){return 0;}

string EV1::getName(){
    return name;
}

string EV1::getOwner(){
    return owner;
}

void EV1::setOwner(string name){
    owner = name;
    ownerList["EV1"] = name;
}


bool EV1::getMortgage(){
    return isMortgage;
}

void EV1::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}


EV1::~EV1(){}
