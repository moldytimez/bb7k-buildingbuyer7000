//
//  bmh.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "bmh.h"
#include "health.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

BMH::BMH():Health(100,180,"BMH"){
    tuitionList[0] = 14;
    tuitionList[1] = 70;
    tuitionList[2] = 200;
    tuitionList[3] = 550;
    tuitionList[4] = 750;
    tuitionList[5] = 950;
}


int BMH::getPurchaseCost(){
    return purchaseCost;
}

int BMH::getImprovementCost(){
    return improvementCost;
}

void BMH::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["BMH"] = improvement;
}


int BMH::getImprovement(){
    return improvement;
}

bool BMH::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["LHI"] == owner && ownerList["OPT"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["LHI"] == owner && ownerList["OPT"] != owner)
            cout << "The property required to be owned is OPT (owner: " << ownerList["OPT"] << ")" << endl;
        if(ownerList["LHI"] != owner && ownerList["OPT"] == owner)
            cout << "The property required to be owned is LHI (owner: " << ownerList["LHI"] << ")" << endl;
        if(ownerList["LHI"] != owner && ownerList["OPT"] != owner){
            cout << "The property required to be owned is LHI (owner: " << ownerList["LHI"] << ")" << endl;
            cout << "The property required to be owned is OPT (owner: " << ownerList["OPT"] << ")" << endl;
        }
    }
    return isMonopoly;
}


bool BMH::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["LHI"] == owner && ownerList["OPT"]== owner) isMonopoly = true;
    return isMonopoly;
}


int BMH::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["LHI"] == 0 && improvementList["OPT"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int BMH::getFees(string ownerPiece){return 0;}

string BMH::getName(){
    return name;
}

string BMH::getOwner(){
    return owner;
}

void BMH::setOwner(string name){
    owner = name;
    ownerList["BMH"] = name;
}


bool BMH::getMortgage(){
    return isMortgage;
}

void BMH::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}


BMH::~BMH(){}
