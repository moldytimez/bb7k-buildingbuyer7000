//
//  mkv.cpp
//  bb7k
//
//  Created by jessica on 14/11/20.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "mkv.h"
#include <iostream>
#include <string>
#include "residence.h"
#include "building.h"
using namespace std;


MKV::MKV():Residence("MKV"){}

string MKV::getOwner(){
    return owner;
}

void MKV::setOwner(string ownerPiece){
    resCount[owner]--;
    owner = ownerPiece;
    resCount[owner]++;
}

int MKV::getFees(){
    return rent;
}

int MKV::getFees(string ownerPiece){
    cout << "-------INFORMATION-------" << endl;
    cout << "Fees to pay depend on number of residences the owner owns" << endl;
    cout << "The owner owns " << resCount[ownerPiece] << " residence(s)" << endl;
    rent = resCount[ownerPiece]*25;
    return rent;
}


int MKV::getPurchaseCost(){
    return purchaseCost;
}


string MKV::getName(){
    return name;
}



bool MKV::getMortgage(){
    return isMortgage;
}

void MKV::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}


int MKV::getImprovementCost(){return 0;};
void MKV::changeImprovement(string act){}
int MKV::getImprovement(){return 0;}
bool MKV::checkMonopoly(bool whetherPrint){ return false;}
bool MKV::checkMonopoly(){return false;}
MKV::~MKV(){}

