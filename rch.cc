//
//  rch.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "rch.h"
#include "eng.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

RCH::RCH():ENG(100,140,"RCH"){
    tuitionList[0] = 10;
    tuitionList[1] = 50;
    tuitionList[2] = 150;
    tuitionList[3] = 450;
    tuitionList[4] = 625;
    tuitionList[5] = 750;
}


int RCH::getPurchaseCost(){
    return purchaseCost;
}

int RCH::getImprovementCost(){
    return improvementCost;
}

void RCH::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["RCH"] = improvement;
}


int RCH::getImprovement(){
    return improvement;
}

bool RCH::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["DWE"] == owner && ownerList["CPH"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["DWE"] == owner && ownerList["CPH"] != owner)
            cout << "The property required to be owned is CPH (owner: " << ownerList["CPH"] << ")" << endl;
        if(ownerList["DWE"] != owner && ownerList["CPH"] == owner)
            cout << "The property required to be owned is DWE (owner: " << ownerList["DWE"] << ")" << endl;
        if(ownerList["DWE"] != owner && ownerList["CPH"] != owner){
            cout << "The property required to be owned is DWE (owner: " << ownerList["DWE"] << ")" << endl;
            cout << "The property required to be owned is CPH (owner: " << ownerList["CPH"] << ")" << endl;
        }
    }
    return isMonopoly;
}


bool RCH::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["DWE"] == owner && ownerList["CPH"]== owner) isMonopoly = true;
    return isMonopoly;
}


int RCH::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["DWE"] == 0 && improvementList["CPH"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int RCH::getFees(string ownerPiece){return 0;}

string RCH::getName(){
    return name;
}

string RCH::getOwner(){
    return owner;
}

void RCH::setOwner(string name){
    owner = name;
    ownerList["RCH"] = name;
}


bool RCH::getMortgage(){
    return isMortgage;
}

void RCH::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

RCH::~RCH(){}


