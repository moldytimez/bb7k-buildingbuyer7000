//
//  eit.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "eit.h"
#include "sci2.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

EIT::EIT():SCI2(200,300,"EIT"){
    tuitionList[0] = 26;
    tuitionList[1] = 130;
    tuitionList[2] = 390;
    tuitionList[3] = 900;
    tuitionList[4] = 1100;
    tuitionList[5] = 1275;
}


int EIT::getPurchaseCost(){
    return purchaseCost;
}

int EIT::getImprovementCost(){
    return improvementCost;
}

void EIT::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["EIT"] = improvement;
}


int EIT::getImprovement(){
    return improvement;
}

bool EIT::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["ESC"] == owner && ownerList["C2"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["ESC"] == owner && ownerList["C2"] != owner)
            cout << "The property required to be owned is C2 (owner: " << ownerList["C2"] << ")" << endl;
        if(ownerList["ESC"] != owner && ownerList["C2"] == owner)
            cout << "The property required to be owned is ESC (owner: " << ownerList["ESC"] << ")" << endl;
        if(ownerList["ESC"] != owner && ownerList["C2"] != owner){
            cout << "The property required to be owned is ESC (owner: " << ownerList["ESC"] << ")" << endl;
            cout << "The property required to be owned is C2 (owner: " << ownerList["C2"] << ")" << endl;
        }
    }
    return isMonopoly;
}


bool EIT::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["ESC"] == owner && ownerList["C2"]== owner) isMonopoly = true;
    return isMonopoly;
}

int EIT::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["ESC"] == 0 && improvementList["C2"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int EIT::getFees(string ownerPiece){return 0;}

string EIT::getName(){
    return name;
}

string EIT::getOwner(){
    return owner;
}

void EIT::setOwner(string name){
    owner = name;
    ownerList["EIT"] = name;
}


bool EIT::getMortgage(){
    return isMortgage;
}

void EIT::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

EIT::~EIT(){}

