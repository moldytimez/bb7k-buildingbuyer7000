//
//  pas.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "pas.h"
#include "arts2.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

PAS::PAS():Arts2(50,100,"PAS"){
    tuitionList[0] = 6;
    tuitionList[1] = 30;
    tuitionList[2] = 90;
    tuitionList[3] = 270;
    tuitionList[4] = 400;
    tuitionList[5] = 550;
}


int PAS::getPurchaseCost(){
    return purchaseCost;
}

int PAS::getImprovementCost(){
    return improvementCost;
}

void PAS::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["PAS"] = improvement;
}


int PAS::getImprovement(){
    return improvement;
}

bool PAS::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["HH"] == owner && ownerList["ECH"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["HH"] == owner && ownerList["ECH"] != owner)
            cout << "The property required to be owned is ECH (owner: " << ownerList["ECH"] << ")" << endl;
        if(ownerList["HH"] != owner && ownerList["ECH"] == owner)
            cout << "The property required to be owned is HH (owner: " << ownerList["HH"] << ")" << endl;
        if(ownerList["HH"] != owner && ownerList["ECH"] != owner){
            cout << "The property required to be owned is HH (owner: " << ownerList["HH"] << ")" << endl;
            cout << "The property required to be owned is ECH (owner: " << ownerList["ECH"] << ")" << endl;
        }
    }
    return isMonopoly;
}



bool PAS::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["HH"] == owner && ownerList["ECH"]== owner) isMonopoly = true;
    return isMonopoly;
}


int PAS::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["HH"] == 0 && improvementList["ECH"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int PAS::getFees(string ownerPiece){return 0;}

string PAS::getName(){
    return name;
}

string PAS::getOwner(){
    return owner;
}

void PAS::setOwner(string name){
    owner = name;
    ownerList["PAS"] = name;
}


bool PAS::getMortgage(){
    return isMortgage;
}

void PAS::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

PAS::~PAS(){}


