//
//  hh.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "hh.h"
#include "arts2.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

HH::HH():Arts2(50,120,"HH"){
    tuitionList[0] = 8;
    tuitionList[1] = 40;
    tuitionList[2] = 100;
    tuitionList[3] = 300;
    tuitionList[4] = 450;
    tuitionList[5] = 600;
}


int HH::getPurchaseCost(){
    return purchaseCost;
}

int HH::getImprovementCost(){
    return improvementCost;
}

void HH::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["HH"] = improvement;
}


int HH::getImprovement(){
    return improvement;
}

bool HH::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["PAS"] == owner && ownerList["ECH"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["PAS"] == owner && ownerList["ECH"] != owner)
            cout << "The property required to be owned is ECH (owner: " << ownerList["ECH"] << ")" << endl;
        if(ownerList["PAS"] != owner && ownerList["ECH"] == owner)
            cout << "The property required to be owned is PAS (owner: " << ownerList["PAS"] << ")" << endl;
        if(ownerList["PAS"] != owner && ownerList["ECH"] != owner){
            cout << "The property required to be owned is PAS (owner: " << ownerList["PAS"] << ")" << endl;
            cout << "The property required to be owned is ECH (owner: " << ownerList["ECH"] << ")" << endl;
        }
    }
    return isMonopoly;
}


bool HH::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["PAS"] == owner && ownerList["ECH"]== owner) isMonopoly = true;
    return isMonopoly;
}


int HH::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["PAS"] == 0 && improvementList["ECH"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int HH::getFees(string ownerPiece){return 0;}

string HH::getName(){
    return name;
}

string HH::getOwner(){
    return owner;
}

void HH::setOwner(string name){
    owner = name;
    ownerList["HH"] = name;
}


bool HH::getMortgage(){
    return isMortgage;
}

void HH::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

HH::~HH(){}


