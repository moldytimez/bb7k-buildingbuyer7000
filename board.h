//
//  board.h
//  bb7k
//
//  Created by jessica on 14/11/24.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#ifndef __BOARD_H__
#define __BOARD_H__
#include "player.h"
#include "building.h"
#include <iostream>
#include <string>
#include <map>

class Board{
public:
    bool give500;
    Player *playerList[7];
    Building *buildingList[40];
    std::map<std::string,int> IDList;
    std::map<std::string,std::string> nameList;
    bool isOwnable[40];
    bool isAcademic[40];
    Board();
    ~Board();
    void addPlayer(int,std::string,std::string,int,int,int);
    void addPlayerList();
    void printI(int);
    void printName1(int);
    void printName2(int);
    void printOwner(int);
    void draw();
    int giveDice();
    void collectOSAP(int);
    void dcTimsLine(int,std::string);
    void goToTims(int,std::string);
    void gooseNesting(int);
    void payTuition(int,std::string);
    void payCoopFee(int);
    void atSLC(int);
    void atNeedlesHall(int);
};


#endif
