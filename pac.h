//
//  pac.h
//  bb7k
//
//  Created by jessica on 14/11/21.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#ifndef __PAC_H__
#define __PAC_H__

#include <iostream>
#include <string>
#include "building.h"
#include "gym.h"
class PAC: public GYM{
public:
    PAC();
    std::string getOwner();
    void setOwner(std::string owner);
    int getFees();
    int getFees(std::string);
    int getPurchaseCost();
    std::string getName();
    bool getMortgage();
    void setMortgage(bool whetherMortgage);
    int getImprovementCost();
    void changeImprovement(std::string act);
    int getImprovement();
    bool checkMonopoly(bool whetherPrint);
    bool checkMonopoly();
    ~PAC();
};

#endif
