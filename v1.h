//
//  v1.h
//  bb7k
//
//  Created by jessica on 14/11/20.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#ifndef __V1_H__
#define __V1_H__

#include <iostream>
#include <string>
#include "residence.h"
#include "building.h"

class V1: public Residence{
public:
    V1();
    std::string getOwner();
    void setOwner(std::string);
    int getFees();
    int getFees(std::string);
    int getPurchaseCost();
    std::string getName();
    bool getMortgage();
    void setMortgage(bool whetherMortgage);
    int getImprovementCost();
    void changeImprovement(std::string act);
    int getImprovement();
    bool checkMonopoly(bool whetherPrint);
    bool checkMonopoly();
    ~V1();
};

#endif