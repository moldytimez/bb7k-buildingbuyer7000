//
//  academic.h
//  bb7k
//
//  Created by jessica on 14/11/22.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#ifndef __ACADEMIC_H__
#define __ACADEMIC_H__

#include <iostream>
#include <string>
#include "building.h"
class Academic:public Building{
protected:
    int improvement;
    int improvementCost;
    int tuitionList[6];
public:
    Academic(int improveCost,int purchaseCost,std::string name);
    ~Academic();
};

#endif
