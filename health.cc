//
//  health.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "health.h"
#include <map>
#include <string>
#include "academic.h"
using namespace std;

map<string,string> Health::ownerList;
map<string,int> Health::improvementList;


Health::Health(int improveCost,int purchaseCost,std::string name):Academic(improveCost,purchaseCost,name){
    ownerList["LHI"] = "BANK";
    ownerList["BMH"] = "BANK";
    ownerList["OPT"] = "BANK";
    improvementList["LHI"] = 0;
    improvementList["BMH"] = 0;
    improvementList["OPT"] = 0;
}


Health::~Health(){}