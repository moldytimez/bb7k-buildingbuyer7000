//
//  building.h
//  bb7k
//
//  Created by jessica on 14/11/20.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#ifndef __BUILDING_H__
#define __BUILDING_H__
#include <iostream>
#include <string>
#include <map>

class Building{
protected:
    int purchaseCost;
    std::string name;
    std::string owner;
    bool isMortgage;
    static std::map<std::string,int> gymCount; // count the number of gyms each player owned
    static std::map<std::string,int> resCount; // count the number of residences each player owned
public:
    Building(int purchaseCost,std::string name);
    virtual int getPurchaseCost() = 0;   // return the purchaseCost field
    virtual std::string getName() = 0;   // return the name field
    virtual std::string getOwner() = 0;  // return the owner field
    virtual void setOwner(std::string) = 0;  // set the owner field
    virtual bool getMortgage() = 0;  // return the isMortgage field
    virtual void setMortgage(bool) = 0;  // set the isMortgage field
    virtual int getImprovementCost() = 0; // return the improvement cost of each building
    virtual void changeImprovement(std::string act) = 0; // 2 acts : increase an improvement using string "in" ;
                                                         //decrease an improvement using string "de"
    virtual int getImprovement() = 0;  // return the improvement of each building
    virtual int getFees() = 0;  // return the tutiton of academic building
    virtual int getFees(std::string)=0;  // return the rent/ usageFee of residence/gym
    virtual bool checkMonopoly(bool whetherPrint) = 0;  // check the monopoly and print
    virtual bool checkMonopoly() = 0;  // check the monopoly only
    virtual ~Building();
};

#endif
