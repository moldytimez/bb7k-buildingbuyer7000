//
//  math.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "math.h"
#include <map>
#include <string>
#include "academic.h"
using namespace std;


map<string,string> Math::ownerList;
map<string,int> Math::improvementList;

Math::Math(int improveCost,int purchaseCost,std::string name):Academic(improveCost,purchaseCost,name){
    ownerList["MC"] = "BANK";
    ownerList["DC"] = "BANK";
    improvementList["MC"] = 0;
    improvementList["DC"] = 0;
}

Math::~Math(){}