//
//  ev3.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "ev3.h"
#include "env.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

EV3::EV3():ENV(150,240,"EV3"){
    tuitionList[0] = 20;
    tuitionList[1] = 100;
    tuitionList[2] = 300;
    tuitionList[3] = 750;
    tuitionList[4] = 925;
    tuitionList[5] = 1100;
}


int EV3::getPurchaseCost(){
    return purchaseCost;
}

int EV3::getImprovementCost(){
    return improvementCost;
}

void EV3::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["EV3"] = improvement;
}


int EV3::getImprovement(){
    return improvement;
}

bool EV3::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["EV1"] == owner && ownerList["EV2"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["EV1"] == owner && ownerList["EV2"] != owner)
            cout << "The property required to be owned is EV2 (owner: " << ownerList["EV2"] << ")" << endl;
        if(ownerList["EV1"] != owner && ownerList["EV2"] == owner)
            cout << "The property required to be owned is EV1 (owner: " << ownerList["EV1"] << ")" << endl;
        if(ownerList["EV1"] != owner && ownerList["EV2"] != owner){
            cout << "The property required to be owned is EV1 (owner: " << ownerList["EV1"] << ")" << endl;
            cout << "The property required to be owned is EV2 (owner: " << ownerList["EV2"] << ")" << endl;
        }
    }
    return isMonopoly;
}


bool EV3::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["EV1"] == owner && ownerList["EV2"]== owner) isMonopoly = true;
    return isMonopoly;
}


int EV3::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["EV1"] == 0 && improvementList["EV2"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int EV3::getFees(string ownerPiece){return 0;}

string EV3::getName(){
    return name;
}

string EV3::getOwner(){
    return owner;
}

void EV3::setOwner(string name){
    owner = name;
    ownerList["EV3"] = name;
}


bool EV3::getMortgage(){
    return isMortgage;
}

void EV3::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

EV3::~EV3(){}



