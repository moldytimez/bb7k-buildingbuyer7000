//
//  lhi.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "lhi.h"
#include "health.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

LHI::LHI():Health(100,180,"LHI"){
    tuitionList[0] = 14;
    tuitionList[1] = 70;
    tuitionList[2] = 200;
    tuitionList[3] = 550;
    tuitionList[4] = 750;
    tuitionList[5] = 950;
}


int LHI::getPurchaseCost(){
    return purchaseCost;
}

int LHI::getImprovementCost(){
    return improvementCost;
}

void LHI::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["LHI"] = improvement;
}


int LHI::getImprovement(){
    return improvement;
}

bool LHI::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["BMH"] == owner && ownerList["OPT"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["BMH"] == owner && ownerList["OPT"] != owner)
            cout << "The property required to be owned is OPT (owner: " << ownerList["OPT"] << ")" << endl;
        if(ownerList["BMH"] != owner && ownerList["OPT"] == owner)
            cout << "The property required to be owned is BMH (owner: " << ownerList["BMH"] << ")" << endl;
        if(ownerList["BMH"] != owner && ownerList["OPT"] != owner){
            cout << "The property required to be owned is BMH (owner: " << ownerList["BMH"] << ")" << endl;
            cout << "The property required to be owned is OPT (owner: " << ownerList["OPT"] << ")" << endl;
        }
    }
    return isMonopoly;
}

bool LHI::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["BMH"] == owner && ownerList["OPT"]== owner) isMonopoly = true;
    return isMonopoly;
}



int LHI::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["BMH"] == 0 && improvementList["OPT"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int LHI::getFees(string ownerPiece){return 0;}

string LHI::getName(){
    return name;
}

string LHI::getOwner(){
    return owner;
}

void LHI::setOwner(string name){
    owner = name;
    ownerList["LHI"] = name;
}


bool LHI::getMortgage(){
    return isMortgage;
}

void LHI::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

LHI::~LHI(){}