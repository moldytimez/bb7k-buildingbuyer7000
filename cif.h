//
//  cif.h
//  bb7k
//
//  Created by jessica on 14/11/21.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#ifndef __CIF_H__
#define __CIF_H__

#include <iostream>
#include <string>
#include "building.h"
#include "gym.h"
class CIF: public GYM{
public:
    CIF();
    std::string getOwner();
    void setOwner(std::string owner);
    int getFees();
    int getFees(std::string);
    int getPurchaseCost();
    std::string getName();
    bool getMortgage();
    void setMortgage(bool whetherMortgage);
    int getImprovementCost();
    void changeImprovement(std::string act);
    int getImprovement();
    bool checkMonopoly(bool whetherPrint);
    bool checkMonopoly();
    ~CIF();
};

#endif
