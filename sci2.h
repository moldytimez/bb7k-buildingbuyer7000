//
//  sci2.h
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#ifndef __SCI2_H__
#define __SCI2_H__

#include <map>
#include <string>
#include "academic.h"

class SCI2:public Academic{
protected:
    static std::map<std::string,std::string> ownerList;
    static std::map<std::string,int> improvementList;
public:
    SCI2(int improveCost,int purchaseCost,std::string name);
    ~SCI2();
};

#endif 
