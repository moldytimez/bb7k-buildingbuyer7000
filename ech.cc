//
//  ech.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "ech.h"
#include "arts2.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

ECH::ECH():Arts2(50,100,"ECH"){
    tuitionList[0] = 6;
    tuitionList[1] = 30;
    tuitionList[2] = 90;
    tuitionList[3] = 270;
    tuitionList[4] = 400;
    tuitionList[5] = 550;
}


int ECH::getPurchaseCost(){
    return purchaseCost;
}

int ECH::getImprovementCost(){
    return improvementCost;
}

void ECH::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["ECH"] = improvement;
}


int ECH::getImprovement(){
    return improvement;
}

bool ECH::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["HH"] == owner && ownerList["PAS"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["HH"] == owner && ownerList["PAS"] != owner)
            cout << "The property required to be owned is PAS (owner: " << ownerList["PAS"] << ")" << endl;
        if(ownerList["HH"] != owner && ownerList["PAS"] == owner)
            cout << "The property required to be owned is HH (owner: " << ownerList["HH"] << ")" << endl;
        if(ownerList["HH"] != owner && ownerList["PAS"] != owner){
            cout << "The property required to be owned is HH (owner: " << ownerList["HH"] << ")" << endl;
            cout << "The property required to be owned is PAS (owner: " << ownerList["PAS"] << ")" << endl;
        }
    }
    return isMonopoly;
}


bool ECH::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["HH"] == owner && ownerList["PAS"]== owner) isMonopoly = true;
    return isMonopoly;
}


int ECH::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["HH"] == 0 && improvementList["PAS"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int ECH::getFees(string ownerPiece){return 0;}

string ECH::getName(){
    return name;
}

string ECH::getOwner(){
    return owner;
}

void ECH::setOwner(string name){
    owner = name;
    ownerList["ECH"] = name;
}


bool ECH::getMortgage(){
    return isMortgage;
}

void ECH::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

ECH::~ECH(){}

