//
//  cif.cpp
//  bb7k
//
//  Created by jessica on 14/11/21.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "cif.h"
#include "gym.h"
#include <string>
using namespace std;

CIF::CIF():GYM("CIF"){}

string CIF::getOwner(){
    return owner;
}

void CIF::setOwner(string ownerPiece){
    if(owner != ownerPiece && owner != "BANK") {
        gymCount[owner]--;
    }
    owner = ownerPiece;
    gymCount[ownerPiece]++;
}


int CIF::getFees(){
    return usageFee;
}

int CIF::getFees(string ownerPiece){
    int dice1 = giveDice();
    int dice2 = giveDice();
    cout << "-------INFORMATION-------" << endl;
    cout << "Two dice will be rolled; fees to pay depend on number of gyms the owner owns" << endl;
    cout << "The owner owns " << gymCount[ownerPiece] << " gym(s)" << endl;
    cout << dice1 << " and " << dice2 << " are rolled" << endl;
    if(gymCount[ownerPiece] == 1)  usageFee = 4 * (dice1 + dice2);
    if(gymCount[ownerPiece] == 2)  usageFee = 10 * (dice1 + dice2);
    return usageFee;
}


int CIF::getPurchaseCost(){
    return purchaseCost;
}

string CIF::getName(){
    return name;
}


bool CIF::getMortgage(){
    return isMortgage;
}

void CIF::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}


int CIF::getImprovementCost(){return 0;};
void CIF::changeImprovement(string act){}
int CIF::getImprovement(){return 0;}
bool CIF::checkMonopoly(bool whetherPrint){ return false;}
bool CIF::checkMonopoly(){return false;}

CIF::~CIF(){}




