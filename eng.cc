//
//  eng.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "eng.h"
#include <map>
#include <string>
#include "academic.h"
using namespace std;



map<string,string> ENG::ownerList;
map<string,int> ENG::improvementList;

ENG::ENG(int improveCost,int purchaseCost,std::string name):Academic(improveCost,purchaseCost,name){
    ownerList["RCH"] = "BANK";
    ownerList["DWE"] = "BANK";
    ownerList["CPH"] = "BANK";
    improvementList["RCH"] = 0;
    improvementList["DWE"] = 0;
    improvementList["CPH"] = 0;
}

ENG::~ENG(){}