//
//  opt.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "opt.h"
#include "health.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

OPT::OPT():Health(100,200,"OPT"){
    tuitionList[0] = 16;
    tuitionList[1] = 80;
    tuitionList[2] = 220;
    tuitionList[3] = 600;
    tuitionList[4] = 800;
    tuitionList[5] = 1000;
}


int OPT::getPurchaseCost(){
    return purchaseCost;
}

int OPT::getImprovementCost(){
    return improvementCost;
}

void OPT::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["OPT"] = improvement;
}


int OPT::getImprovement(){
    return improvement;
}

bool OPT::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["LHI"] == owner && ownerList["BMH"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["LHI"] == owner && ownerList["BMH"] != owner)
            cout << "The property required to be owned is BMH (owner: " << ownerList["BMH"] << ")" << endl;
        if(ownerList["LHI"] != owner && ownerList["BMH"] == owner)
            cout << "The property required to be owned is LHI (owner: " << ownerList["LHI"] << ")" << endl;
        if(ownerList["LHI"] != owner && ownerList["BMH"] != owner){
            cout << "The property required to be owned is LHI (owner: " << ownerList["LHI"] << ")" << endl;
            cout << "The property required to be owned is BMH (owner: " << ownerList["BMH"] << ")" << endl;
        }
    }
    return isMonopoly;
}


bool OPT::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["LHI"] == owner && ownerList["BMH"]== owner) isMonopoly = true;
    return isMonopoly;
}


int OPT::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["LHI"] == 0 && improvementList["BMH"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int OPT::getFees(string ownerPiece){return 0;}

string OPT::getName(){
    return name;
}

string OPT::getOwner(){
    return owner;
}

void OPT::setOwner(string name){
    owner = name;
    ownerList["OPT"] = name;
}


bool OPT::getMortgage(){
    return isMortgage;
}

void OPT::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

OPT::~OPT(){}