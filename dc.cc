//
//  dc.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "dc.h"
#include "math.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

DC::DC():Math(200,400,"DC"){
    tuitionList[0] = 50;
    tuitionList[1] = 200;
    tuitionList[2] = 600;
    tuitionList[3] = 1400;
    tuitionList[4] = 1700;
    tuitionList[5] = 2000;
}


int DC::getPurchaseCost(){
    return purchaseCost;
}

int DC::getImprovementCost(){
    return improvementCost;
}

void DC::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["DC"] = improvement;
}


int DC::getImprovement(){
    return improvement;
}

bool DC::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["MC"] == owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        cout << "The property required to be owned is MC (owner: " << ownerList["MC"]<< ")" << endl;
    }
    return isMonopoly;
}

bool DC::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["MC"] == owner) isMonopoly = true;
    return isMonopoly;
}

int DC::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["MC"] == 0) tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}


int DC::getFees(string ownerPiece){return 0;}

string DC::getName(){
    return name;
}

string DC::getOwner(){
    return owner;
}

void DC::setOwner(string name){
    owner = name;
    ownerList["DC"] = name;
}


bool DC::getMortgage(){
    return isMortgage;
}

void DC::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}


DC::~DC(){}
