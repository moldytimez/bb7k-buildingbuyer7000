//
//  dwe.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "dwe.h"
#include "eng.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

DWE::DWE():ENG(100,140,"DWE"){
    tuitionList[0] = 10;
    tuitionList[1] = 50;
    tuitionList[2] = 150;
    tuitionList[3] = 450;
    tuitionList[4] = 625;
    tuitionList[5] = 750;
}


int DWE::getPurchaseCost(){
    return purchaseCost;
}

int DWE::getImprovementCost(){
    return improvementCost;
}

void DWE::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["DWE"] = improvement;
}


int DWE::getImprovement(){
    return improvement;
}

bool DWE::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["RCH"] == owner && ownerList["CPH"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["RCH"] == owner && ownerList["CPH"] != owner)
            cout << "The property required to be owned is CPH (owner: " << ownerList["CPH"] << ")" << endl;
        if(ownerList["RCH"] != owner && ownerList["CPH"] == owner)
            cout << "The property required to be owned is RCH (owner: " << ownerList["RCH"] << ")" << endl;
        if(ownerList["RCH"] != owner && ownerList["CPH"] != owner){
            cout << "The property required to be owned is RCH (owner: " << ownerList["RCH"] << ")" << endl;
            cout << "The property required to be owned is CPH (owner: " << ownerList["CPH"] << ")" << endl;
        }
    }
    return isMonopoly;
}


bool DWE::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["RCH"] == owner && ownerList["CPH"]== owner) isMonopoly = true;
    return isMonopoly;
}


int DWE::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["RCH"] == 0 && improvementList["CPH"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int DWE::getFees(string ownerPiece){return 0;}

string DWE::getName(){
    return name;
}

string DWE::getOwner(){
    return owner;
}

void DWE::setOwner(string name){
    owner = name;
    ownerList["DWE"] = name;
}


bool DWE::getMortgage(){
    return isMortgage;
}

void DWE::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

DWE::~DWE(){}