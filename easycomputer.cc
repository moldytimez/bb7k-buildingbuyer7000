#include "player.h"
#include "easycomputer.h"
#include "building.h"
#include <string>
#include <iostream>
#include <sstream>
using namespace std;

EasyComputer::EasyComputer(int playerID,string name,string piece,int savings, int position,int rurc, Building **buildingList):Player(playerID,name,piece,savings,position,rurc,buildingList) {}

EasyComputer::~EasyComputer() {}

int EasyComputer::getID() { return playerID; }

string EasyComputer::getName() { return name; }

string EasyComputer::getPiece() { return piece; }

int EasyComputer::getSavings() { return savings; }

void EasyComputer::setSavings(int s) { savings = s; }

int EasyComputer::getPosition() { return position; }

void EasyComputer::setPosition(int buildingID) { position = buildingID; }

void EasyComputer::changePosition(int change) {
  position += change;
  if (position < 0) { position += 40; }
  if (position >= 40) { position -= 40; }
  cout << piece << " (" << name << ") have moved to ";
  if (isOwnable[position]) { cout << buildingList[position]->getName(); }
  else if (position==0) { cout << "COLLECT OSAP"; }
  else if (position==2||position==17||position==33) { cout << "SLC"; }
  else if (position==4) { cout << "TUITION"; }
  else if (position==7||position==22||position==36) { cout << "NEEDLES HALL"; }
  else if (position==10) { cout << "DC Tims Line"; }
  else if (position==20) { cout << "Goose Nesting"; }
  else if (position==30) { cout << "GO TO TIMS"; }
  else if (position==38) { cout << "COOP FEE"; }
  
  cout << " by moving " << change << " squares" << endl;
}

int EasyComputer::getDCStatus() { return dcStatus; }

void EasyComputer::setDCStatus(int value) { dcStatus = value; }

int EasyComputer::getRurc() { return rurc; }

void EasyComputer::changeRurc(int change) { rurc+=change; }

void EasyComputer::changeSavings(int change) {
  savings += change;
  cout << piece << " (" << name << ") ";
  if (change > 0) { cout << "got $" << change << " "; }
  else { cout << "paid $" << -change << " "; }
}

void EasyComputer::pay(int amount, int ownerID) {
  string ownerPiece = playerList[ownerID]->getPiece();
  string ownerName = playerList[ownerID]->getName();
  string printName = ownerPiece + " (" + ownerName + ")";
  if (savings > amount) {
    cout << "-------INFORMATION-------" << endl;
    changeSavings(-amount);
    cout << "to " << printName << endl;
    playerList[ownerID]->changeSavings(amount);
    cout << "from " << piece << " (" << name << ")" << endl;
    cout << "---------------------" << endl << endl;
  } else {
    bankruptProcess(amount,ownerID);
  }
}

void EasyComputer::buy(int buildingID) {
  string buildingName = buildingList[buildingID]->getName();
  string ownerPiece = buildingList[buildingID]->getName();
  string ownerName = nameList[ownerPiece];
  int price = buildingList[buildingID]->getPurchaseCost();
  bool isMortgaged = buildingList[buildingID]->getMortgage();

  if (!isMortgaged && savings > price) {
    cout << "-------INFORMATION-------" << endl;
    buildingList[buildingID]->setOwner(piece);
    changeSavings(-price);
    cout << "to buy " << buildingName << endl;
    cout << buildingName << " is now owned by ";
    cout << piece << " (" << name << ")" << endl << endl;
  }
}

void EasyComputer::changeImprovement(int buildingID,char action) {
  string buildingName = buildingList[buildingID]->getName();
  string ownerPiece = buildingList[buildingID]->getOwner();
  string ownerName = nameList[ownerPiece];
  int price = buildingList[buildingID]->getImprovementCost();
  int improvement = buildingList[buildingID]->getImprovement();
  bool isMortgaged = buildingList[buildingID]->getMortgage();
  bool isMonopoly = buildingList[buildingID]->checkMonopoly();

  // if buy improvement
  if (action == '+') {
    if (!isMortgaged && savings > price && improvement != 5 && isMonopoly) {
      cout << "-------INFORMATION-------" << endl;
      changeSavings(-price);
      cout << "to upgrade " << buildingName << endl;
      buildingList[buildingID]->changeImprovement("in");
      cout << buildingName << " is now of level " << improvement+1 << endl;
      cout << "---------------------" << endl;
    }
  
  // if sell improvement
  } else if (action == '-') {
    if (!isMortgaged && improvement != 0) {
      cout << "-------INFORMATION-------" << endl;
      changeSavings(price/2);
      cout << "by degrading " << buildingName << endl;
      buildingList[buildingID]->changeImprovement("de");
      cout << buildingName << " is now of level " << improvement-1 << endl;
      cout << "---------------------" << endl << endl;
    }
  }
}

void EasyComputer::changeMortgage(int buildingID,char action) {
  string buildingName = buildingList[buildingID]->getName();
  string ownerPiece = buildingList[buildingID]->getOwner();
  string ownerName = nameList[ownerPiece];
  int price = buildingList[buildingID]->getPurchaseCost()/2;
  int improvement = buildingList[buildingID]->getImprovement();
  bool isMortgaged = buildingList[buildingID]->getMortgage();

  // if mortgage
  if (action == '+') {
    if (!isMortgaged && improvement == 0) {
      cout << "-------INFORMATION-------" << endl;
      changeSavings(price);
      cout << "by mortgaging " << buildingName << endl;
      buildingList[buildingID]->setMortgage(true);
      cout << buildingName << " is mortgaged" << endl;
    }

  // if unmortgage
  } else if (action == '-') {
    if (isMortgaged && savings >= price*1.1) {
      changeSavings(-(price*1.1));
      cout << "to unmortgage " << buildingName << endl;
      buildingList[buildingID]->setMortgage(false);
      cout << piece << " (" << name << ") paid $" << price*1.1 << " to unmortgage";
      cout << buildingName << endl;
      cout << buildingName << " is unmortgaged" << endl;
    }
  }
}

void EasyComputer::giveAsset() {}

void EasyComputer::bankruptProcess(int amount, int ownerID) {
  cout << piece << "(" << name << ") declared bankruptcy" << endl;
  setBankruptcy(true);
  string ownerPiece = playerList[ownerID]->getPiece();
  string ownerName = playerList[ownerID]->getName();
  string printName = ownerPiece + " (" + ownerName + ")";
  string input = "";
  // change owner
  for (int i=0; i<40; i++) {
    if (isOwnable[i] && buildingList[i]->getOwner() == piece) {
      string buildingName = buildingList[i]->getName();
      buildingList[i]->setOwner(ownerPiece);
      cout << "-------INFORMATION-------" << endl;
      cout << buildingName << " is now owned by " << printName << endl;
      
      // for building mortgaged and the fees is not paid to bank
      if (buildingList[i]->getMortgage() && ownerID != 0) {
        cout << "Since " << buildingName << " is mortgaged, ";
        cout << printName << " must pay 10% of its price to the BANK: ";
        int cost = buildingList[i]->getPurchaseCost();
        int price1 = 0.1*cost;
        cout << "$" << price1 << endl;
        playerList[ownerID]->pay(price1,0);

        string decision = "mortgage";
        playerList[ownerID]->strategy(decision);
        int price2 = cost/2;
        int price3 = price2*1.1;
        if (decision == "mortgage") {
          cout << "-------SELECTION-------" << endl;
          cout << "You may choose to unmortgage the property right now ";
          cout << "by paying $" << price2 << endl;
          cout << "OR you may choose to unmortgage the property later ";
          cout << "by paying $" << price3 << " at that time" << endl;
          while (1) {
            cout << "-------CONFIRMATION-------" << endl;
            cout << "Please confirm with 'now' or 'later' ";
            cout << "(other input will not be recognized): ";
            string input = "mortgage";
            playerList[ownerID]->strategy(input);
            cin >> input;
            
            if (cin.eof()) {
              cin.clear();
              continue;
            }
            if (input != "now" && input != "later") {
              continue;
            }          
            if (input == "now") {
              playerList[ownerID]->pay(price2,0);
              break; 
            } else if (input == "later") {
              cout << printName << " did not unmortgage " << buildingName << endl;
              break;
            }
          }
        }
      }
    }
  }
  cout << "---------------------" << endl;
  return;
}


bool EasyComputer::getBankruptcy() { return isBankrupt; }

void EasyComputer::setBankruptcy(bool bankrupt) { isBankrupt = bankrupt; }


void EasyComputer::strategy(string &decision) {
  // if other player declared bankruptcy and owned owner is current COM
  if (decision == "bankruptMortgage") {
    if (savings > 300) { decision = "now"; }
    else { decision = "later"; }
    return;
  }

  if (decision != "" && decision != "action") {
  // if requested trade
  stringstream ss(decision);
  string oppoPiece,give,receive;
  int money;
  if (ss >> oppoPiece && ss >> give && ss >> receive) {
    stringstream ssG(give);
    stringstream ssR(receive);

    if (ssG >> money) {
      if (savings > money+300) { decision = "yes"; }
      else { decision = "no"; }
    }

    else if (ssR >> money) {
      for (int i=0; i<40; i++) {
        if (isAcademic[i] && give == buildingList[i]->getName()) {
          int cost = buildingList[i]->getPurchaseCost();
          int impr = buildingList[i]->getImprovement();
          int imprCost = buildingList[i]->getImprovementCost();
          int value = cost + impr*imprCost;
          if (money >= value) {
            decision = "yes";
            return;
          }
          else {
            decision = "no";
            return;
          }
        }
      }
    }
  } // end processing trade
  
  } // end decision != ""
  
  // normal process
  // get required info for easier following process
  bool ownedList[40];             // true if owned property, false otherwise
  bool ownedAcaList[40];          // true if owned academic block, false otherwise
  for (int i=0; i<40; i++) {
    if (isOwnable[i] && buildingList[i]->getOwner() == piece) {
      ownedList[i] = true;
    } else { ownedList[i] = false; }
    
    if (isAcademic[i] && buildingList[i]->getOwner() == piece) {
      ownedAcaList[i] = true;
    } else { ownedAcaList[i] = false; }
  }

  // decision of buying the property or not
  if (isOwnable[position] && buildingList[position]->getOwner() == "BANK") {
    int price = buildingList[position]->getPurchaseCost();
    if (savings > price+100) {
      buy(position);
    }
  }

  if (decision == "action") {
  // decision of improving the property or not
  for (int i=0; i<10; i++) {
    if (ownedAcaList[i] && savings > 300) { changeImprovement(i,'+'); }
  }
  for (int i=10; i<20; i++) {
    if (ownedAcaList[i] && savings > 400) { changeImprovement(i,'+'); }
  }
  for (int i=20; i<30; i++) {
    if (ownedAcaList[i] && savings > 500) { changeImprovement(i,'+'); }
  }
  for (int i=30; i<40; i++) {
    if (ownedAcaList[i] && savings > 600) { changeImprovement(i,'+'); }
  }
  } // end decision == action

  // if non-property building
  // if at TUITION
  if (position == 4 && decision != "done" && decision != "action") {
    int totalWorth = savings;
    for (int i=0; i<40; i++) {
      if (ownedList[i]) {
        totalWorth += buildingList[i]->getPurchaseCost();
        if (ownedAcaList[i]) {
          int improvement = buildingList[i]->getImprovement();
          totalWorth += buildingList[i]->getImprovementCost()*improvement;
        }
      }
    }
    if (totalWorth*0.1 > 300) { decision = "amount"; }
    else { decision = "percentage"; }

    // if at DC Tims Line
  } else if ((position == 30 || position == 10) &&
             decision != "action" && decision != "done")  {
    if (rurc != 0) { decision = "rimcup"; }
    else { decision = "dice"; }
  }
}
