//
//  mc.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "mc.h"
#include "math.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

MC::MC():Math(200,350,"MC"){
    tuitionList[0] = 35;
    tuitionList[1] = 175;
    tuitionList[2] = 500;
    tuitionList[3] = 1100;
    tuitionList[4] = 1300;
    tuitionList[5] = 1500;
}


int MC::getPurchaseCost(){
    return purchaseCost;
}

int MC::getImprovementCost(){
    return improvementCost;
}

void MC::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["MC"] = improvement;
}


int MC::getImprovement(){
    return improvement;
}

bool MC::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["DC"] == owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        cout << "The property required to be owned is DC (owner: " << ownerList["DC"]<< ")" << endl;
    }
    return isMonopoly;
}


bool MC::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["DC"] == owner) isMonopoly = true;
    return isMonopoly;
}



int MC::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["DC"] == 0) tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}


int MC::getFees(string ownerPiece){return 0;}

string MC::getName(){
    return name;
}

string MC::getOwner(){
    return owner;
}

void MC::setOwner(string name){
    owner = name;
    ownerList["MC"] = name;
}


bool MC::getMortgage(){
    return isMortgage;
}

void MC::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

MC::~MC(){}
