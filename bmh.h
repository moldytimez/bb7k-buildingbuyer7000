//
//  bmh.h
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#ifndef __BMH_H__
#define __BMH_H__

#include <iostream>
#include <string>
#include <map>
#include "health.h"

class BMH:public Health{
public:
    BMH();
    int getPurchaseCost();
    int getImprovementCost();
    void changeImprovement(std::string act);
    int getImprovement();
    bool checkMonopoly(bool whetherPrint);
    bool checkMonopoly();
    int getFees();
    int getFees(std::string);
    std::string getName();
    std::string getOwner();
    void setOwner(std::string name);
    bool getMortgage();
    void setMortgage(bool whetherMortgage);
    ~BMH();
};

#endif
