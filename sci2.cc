//
//  sci2.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "sci2.h"
#include <map>
#include <string>
#include "academic.h"
using namespace std;

map<string,string> SCI2::ownerList;
map<string,int> SCI2::improvementList;


SCI2::SCI2(int improveCost,int purchaseCost,std::string name):Academic(improveCost,purchaseCost,name){
    ownerList["EIT"] = "BANK";
    ownerList["ESC"] = "BANK";
    ownerList["C2"] = "BANK";
    improvementList["EIT"] = 0;
    improvementList["ESC"] = 0;
    improvementList["C2"] = 0;
}

SCI2::~SCI2(){}