#ifndef __HUMAN_H__
#define __HUMAN_H__
#include "player.h"
#include <string>

class Human: public Player {
 public:
  Human(int,std::string,std::string,int,int,int,Building**);
  ~Human();
  int getID();
  std::string getName();
  std::string getPiece();
  int getSavings();
  void setSavings(int);
  int getPosition();
  void setPosition(int);
  void changePosition(int);
  int getDCStatus();
  void setDCStatus(int);
  int getRurc();
  void changeRurc(int);
  void changeSavings(int);
  void pay(int,int);
  void buy(int);
  void changeImprovement(int,char);
  void changeMortgage(int,char);
  void giveAsset();
  void bankruptProcess(int,int);
  bool getBankruptcy();
  void setBankruptcy(bool);
  void strategy(std::string&);
};
#endif
