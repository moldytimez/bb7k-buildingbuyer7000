#include "player.h"
#include "human.h"
#include "building.h"
#include <string>
#include <iostream>
using namespace std;

Human::Human(int playerID, string name, string piece, int savings, int position,int rurc,Building **buildingList):Player(playerID,name,piece,savings,position,rurc,buildingList) {}

Human::~Human() {}

int Human::getID() { return playerID; }

string Human::getName() { return name; }

string Human::getPiece() { return piece; }

int Human::getSavings() { return savings; }

void Human::setSavings(int s) { savings = s; }

int Human::getPosition() { return position; }

void Human::setPosition(int buildingID) { position = buildingID; }

void Human::changePosition(int change) {
  position = position + change;
  if (position < 0) { position = position+40; }
  if (position >= 40) { position = position-40; }
  string temp = piece;
  cout << temp << " (" <<  name << ") have moved to ";
  if (isOwnable[position]) { cout << buildingList[position]->getName(); }
  else if (position==0) { cout << "COLLECT OSAP"; }
  else if (position==2||position==17||position==33) { cout << "SLC"; }
  else if (position==4) { cout << "TUITION"; }
  else if (position==7||position==22||position==36) { cout << "NEEDLES HALL"; }
  else if (position==10) { cout << "DC Tims Line"; }
  else if (position==20) { cout << "Goose Nesting"; }
  else if (position==30) { cout << "GO TO TIMS"; }
  else if (position==38) { cout << "COOP FEE"; }
  cout << " by moving " << change << " squares" << endl;
}

int Human::getDCStatus() { return dcStatus; }

void Human::setDCStatus(int value) { dcStatus = value; }

int Human::getRurc() { return rurc; }

void Human::changeRurc(int change) { rurc+=change; }

void Human::changeSavings(int change) {
  savings += change;
  cout << piece << " (" << name << ") ";
  if (change > 0) { cout << "got $" << change << " "; }
  else { cout << "paid $" << -change << " "; }
}

void Human::pay(int amount, int ownerID) {
  string ownerPiece = playerList[ownerID]->getPiece();
  string ownerName = playerList[ownerID]->getName();
  string printName = ownerPiece + " (" + ownerName + ")";
  if (savings > amount) {
    cout << "-----INFORMATION-----" << endl;
    changeSavings(-amount);
    cout << "to " << printName << endl;
    playerList[ownerID]->changeSavings(amount);
    cout << "from " << piece << " (" << name << ")" << endl;
    cout << printName << "'s savings changes to $" << savings << endl;
    cout << "---------------" << endl << endl;
  } else {
    cout << "-------WARNING-------" << endl;
    cout << "You lack $" << amount-savings << " to pay to " << ownerPiece;
    cout << " (" << ownerName << ")" << endl;
    cout << "You can sell improvements or mortgage owned property" << endl;
    cout << "Or you can declare bankruptcy" << endl;
    bankruptProcess(amount,ownerID);
  }
}

void Human::buy(int buildingID) {
  string buildingName = buildingList[buildingID]->getName();
  string ownerPiece = buildingList[buildingID]->getName();
  string ownerName = nameList[ownerPiece];
  int price = buildingList[buildingID]->getPurchaseCost();
  bool isMortgaged = buildingList[buildingID]->getMortgage();

  // if mortgaged
  if (isMortgaged) {
    cout << "-------WARNING-------" << endl;
    cout << buildingName << " is mortgaged by " << ownerPiece << "(";
    cout << ownerName << ")" << endl;
    cout << "You cannot buy this property" << endl;
    cout << "---------------------" << endl << endl;
  }
  
  // if do not have enough money
  else if (savings < price) {
    cout << "-------WARNING-------" << endl;
    cout << "You lack $" << price-savings << " to buy " << buildingName << endl;
    cout << "---------------------" << endl << endl;
  }

  else {
    while (1) {
      cout << "-------CONFIRMATION-------" << endl;
      cout << "You can pay $" << price << " to buy " << buildingName << endl;
      cout << "Please confirm with 'yes' or 'no' ";
      cout << "(other input will not be recognized): ";
      string input;
      cin >> input;
      if (!cin) {
        cin.clear();
        cin.ignore();
        continue;
      } else if (input == "yes") {
        cout << "-------INFORMATION-------" << endl;
        buildingList[buildingID]->setOwner(piece);
        changeSavings(-price);
        cout << "to buy " << buildingName << endl;
        cout << buildingName << " is now owned by ";
        cout << piece << " (" << name << ")" << endl;
        break;
      } else if (input == "no") {
        cout << "You did not buy " << buildingName << endl;
        break;
      } else { continue; }
    }
    cout << "---------------------" << endl << endl;
  }
}

void Human::changeImprovement(int buildingID, char action) {

  // if not academic building
  if (!isAcademic[buildingID]) {
    cout << "-------WARNING-------" << endl;
    cout << "Only Academic building can be upgraded" << endl;
    cout << "---------------------" << endl << endl;
    return;
  }
  
  string buildingName = buildingList[buildingID]->getName();
  string ownerPiece = buildingList[buildingID]->getOwner();
  string ownerName = nameList[ownerPiece];
  int price = buildingList[buildingID]->getImprovementCost();
  int improvement = buildingList[buildingID]->getImprovement();
  bool isMortgaged = buildingList[buildingID]->getMortgage();
  string tempPiece = piece;

  // if buy improvement
  if (action == '+') {

    // if owner is other player/BANK
    if (ownerPiece != tempPiece) {
      cout << "-------WARNING-------" << endl;
      cout << buildingName << " is not owned by you" << endl;
      cout << "It is owned by " << ownerPiece << " (" << ownerName << ")" << endl;
      cout << "---------------------" << endl << endl;
    }

    // if mortgaged
    else if (isMortgaged) {
      cout << "-------WARNING-------" << endl;
      cout << "You need to unmortgage " << buildingName << "first" << endl;
      cout << "---------------------" << endl << endl;
    }
    
    // if do not have enough money
    else if (savings < price) {
      cout << "-------WARNING-------" << endl;
      cout << "You lack $" << price-savings << " to build up improvements" << endl;
      cout << "---------------------" << endl << endl;
    }

    // if has already improved 5 times
    else if (improvement == 5) {
      cout << "-------WARNING-------" << endl;
      cout << "You cannot build up more improvements" << endl;
      cout << "---------------------" << endl << endl;
    }

    // if not own monopoly
    else if (!buildingList[buildingID]->checkMonopoly()) {
      buildingList[buildingID]->checkMonopoly(true); // print required property
    }

    else {
      cout << "-------CONFIRMATION-------" << endl;
      cout << "You will pay $" << price << " to upgrade " << buildingName << " ";
      cout << "from level " << improvement << " to " << improvement+1 << endl;
      cout << "Please confirm with 'yes' ";
      cout << "(other input will be recognized as 'no'): ";
      string input;
      cin >> input;
      if (!cin) {
        cin.clear();
        cout << "Invalid input. Operation aborted" << endl;
        return;
      } else {
        if (input == "yes") {
        cout << "-------INFORMATION-------" << endl;
        changeSavings(-price);
        cout << "to upgrade " << buildingName << endl;
        buildingList[buildingID]->changeImprovement("in");
        cout << buildingName << " is now of level " << improvement+1 << endl;
      } else {
        cout << "You did not upgrade " << buildingName << endl;
      }
      cout << "---------------------" << endl << endl;
      }
    }
  
  // if sell improvement
  } else if (action == '-') {

    // if owner is other player/BANK
    if (ownerPiece != tempPiece) {
      cout << "-------WARNING-------" << endl;
      cout << buildingName << " is not owned by you" << endl;
      cout << "It is owned by " << ownerPiece << " (" << ownerName << ")" << endl;
      cout << "---------------------" << endl << endl;
    }
    
    // if mortgaged
    if (isMortgaged) {
      cout << "-------WARNING-------" << endl;
      cout << "You need to unmortgage " << buildingName << "first" << endl;
      cout << "---------------------" << endl << endl;
    }
    
    // if never improved
    else if (improvement == 0) {
      cout << "-------WARNING-------" << endl;
      cout << "You have not improved " << buildingName << endl;
      cout << "---------------------" << endl << endl;
    }

    else {
      cout << "-------CONFIRMATION-------" << endl;
      cout << "You will get $" << price/2 << " if you degrade " << buildingName;
      cout << " from level " << improvement << " to " << improvement-1 << endl;
      cout << "Please confirm with 'yes' ";
      cout << "(other input will be recognized as 'no'): ";
      string input;
      cin >> input;
      if (!cin) {
        cin.clear();
        cout << "Invalid input. Operation aborted" << endl;
        return;
      }

      if (input == "yes") {
        cout << "-------INFORMATION-------" << endl;
        changeSavings(price/2);
        cout << "by degrading " << buildingName << endl;
        buildingList[buildingID]->changeImprovement("de");
        cout << buildingName << " is now of level " << improvement-1 << endl;
      } else {
        cout << "You did not degrade " << buildingName << endl;
      }
      cout << "---------------------" << endl << endl;
    }
  }
}

void Human::changeMortgage(int buildingID,char action) {

  // if not ownable
  if (!isOwnable[buildingID]) {
    cout << "-------WARNING-------" << endl;
    cout << "Only ownable property can be mortgaged" << endl;
    cout << "---------------------" << endl;
    return;
  }

  string buildingName = buildingList[buildingID]->getName();
  string ownerPiece = buildingList[buildingID]->getOwner();
  string ownerName = nameList[ownerPiece];
  int price = buildingList[buildingID]->getPurchaseCost();
  int improvement = buildingList[buildingID]->getImprovement();
  bool isMortgaged = buildingList[buildingID]->getMortgage();
  string tempPiece = piece;

  // if mortgage
  if (action == '+') {

    // if not owner
    if (ownerPiece != tempPiece) {
      cout << "-------WARNING-------" << endl;
      cout << buildingName << "is not owned by you" << endl;
      cout << "It is owned by " << ownerPiece << " (" << ownerName << ")" << endl;
      cout << "---------------------" << endl << endl;
    }
    
    // if mortgaged
    else if (isMortgaged) {
      cout << "-------WARNING-------" << endl;
      cout << "You have already mortgaged " << buildingName << endl;
      cout << "---------------------" << endl << endl;
    }
    
    // if not sold improvement yet
    else if (improvement != 0) {
      cout << "-------WARNING-------" << endl;
      cout << "You need sell improvement of " << buildingName << " first" << endl;
      cout << buildingName << " is of level " << improvement << endl;
      cout << "--------------------" << endl << endl;
    }
    
    else {
      cout << "-------CONFIRMATION-------" << endl;
      cout << "You will get $" << price/2 << " if you mortgage " << buildingName;
      cout << endl;
      cout << "Please confirm with 'yes' ";
      cout << "(other input will be recognized as 'no'): ";
      string input;
      cin >> input;
      if (!cin) {
        cin.clear();
        cout << "Invalid input. Operation aborted" << endl;
        return;
      }
    
      if (input == "yes") {
        cout << "-------INFORMATION-------" << endl;
        changeSavings(price/2);
        cout << "by mortgaging " << buildingName << endl;
        buildingList[buildingID]->setMortgage(true);
        cout << buildingName << " is mortgaged" << endl;
      } else {
        cout << "You did not mortgage " << buildingName << endl;
      }
      cout << "---------------------" << endl << endl;
    }
  }

  // if unmortgage
  else if (action == '-') {

    // if owner is other player/BANK
    if (ownerPiece != tempPiece) {
      cout << "-------WARNING-------" << endl;
      cout << buildingName << "is not owned by you" << endl;
      cout << "It is owned by " << ownerPiece << " (" << ownerName << ")" << endl;
      cout << "---------------------" << endl << endl;
    }

    // if not mortgaged
    else if (!isMortgaged) {
      cout << "-------WARNING-------" << endl;
      cout << buildingName << " is not mortgaged" << endl;
      cout << "---------------------" << endl << endl;
    }

    // if not enough money
    else if (savings < (price/2)*1.1) {
      cout << "-------WARNING-------" << endl;
      cout << "You lack $" << (price/2)*1.1-savings << " to unmortgage ";
      cout << buildingName << endl;
      cout << "---------------------" << endl << endl;
    }

    else {
      cout << "-------CONFIRMATION-------" << endl;
      cout << "You will pay $" << (price/2)*1.1 << " to unmortgage ";
      cout << buildingName << endl;
      cout << "Please confirm with 'yes' ";
      cout << "(other input will be recognized as 'no'): ";
      string input;
      cin >> input;
      if (!cin) {
        cin.clear();
        cout << "Invalid input. Operation aborted" << endl;
        return;
      }

      if (input == "yes") {
        cout << "-------INFORMATION-------" << endl;
        changeSavings(-((price/2)*1.1));
        cout << "to unmortgage " << buildingName << endl;
        buildingList[buildingID]->setMortgage(false);
        cout << buildingName << " is unmortgaged" << endl;
      } else {
        cout << "You did not unmortgage " << buildingName << endl;
      }
      cout << "---------------------" << endl << endl;
    }
  }
}

void Human::giveAsset() {
  cout << "--------------------" << endl;
  cout << "Piece: " << piece;
  cout << "     Player name: " << name << endl;
  cout << "Savings: " << savings << endl;
  cout << "Owned Property: " << endl;
  for (int i=0; i<40; i++) {
    int count = 0;
    if (isOwnable[i] && buildingList[i]->getOwner() == piece) {
      count++;
      cout << buildingList[i]->getName();
      if (isAcademic[i]) {
        int improvement = buildingList[i]->getImprovement();
        cout << " (upgraded: " << improvement << " time(s))";
      }
      if (count%3 == 0) { cout << endl; }
      else { cout << "     "; }
    }
  }
  cout << endl << "Mortgaged Property: " << endl;
  for (int i=0; i<40; i++) {
    if (isOwnable[i] && buildingList[i]->getOwner() == piece &&
        buildingList[i]->getMortgage()) {
      cout << buildingList[i]->getName() << " ";
    }
  }
  cout << endl << "--------------------" << endl << endl;
}

void Human::bankruptProcess(int amount, int ownerID) {
  string input,buildingName;
  int buildingID = 0;
  string ownerPiece = playerList[ownerID]->getPiece();
  string ownerName = playerList[ownerID]->getName();
  string printName = ownerPiece + " (" + ownerName + ")";

  while (1) {
  cout << "Please type a command: ";
  cin >> input;
    if (!cin) {
      cin.clear();
      cin.ignore();
      cout << "Invalid input. Please try again" << endl;
      continue;
    }

    if (input == "bankrupt") {
      cout << "-------CONFIRMATION-------" << endl;
      cout << "You declared bankruptcy" << endl;
      cout << "Please confirm with 'yes' ";
      cout << "(other input will be recognized as 'no'): ";
      cin >> input;
      if (!cin) {
        cin.clear();
        cin.ignore();
        cout << "Invalid input. Please try again" << endl;
        continue;
      } else if (input == "yes") {
        cout << "-------INFORMATION-------" << endl;
        cout << piece << " (" << name << ") declared bankruptcy" << endl;
        cout << "-------------------------" << endl;
        setBankruptcy(true);

        // change owner
        for (int i=0; i<40; i++) {
          if (isOwnable[i] && buildingList[i]->getOwner() == piece) {
            string buildingName = buildingList[i]->getName();
            buildingList[i]->setOwner(ownerPiece);
            cout << "-------INFORMATION-------" << endl;
            cout << buildingName << " is now owned by " << printName << endl;

            // for building mortgaged and the fees is not paid to bank
            if (buildingList[i]->getMortgage() && ownerID != 0) {
              cout << "Since " << buildingName << " is mortgaged, ";
              cout << printName << " must pay 10% of its price to the BANK: ";
              int cost = buildingList[i]->getPurchaseCost();
              int price1 = 0.1*cost;
              cout << "$" << price1 << endl;
              playerList[ownerID]->pay(price1,0);

              string decision = "bankruptMortgage";
              playerList[ownerID]->strategy(decision);
              int price2 = cost/2;
              int price3 = price2*1.1;
                
              if (decision == "bankruptMortgage") {
                cout << "-------SELECTION-------" << endl;
                cout << "You may choose to unmortgage the property right now ";
                cout << "by paying $" << price2 << endl;
                cout << "OR you may choose to unmortgage the property later ";
                cout << "by paying $" << price3 << " at that time" << endl;
                
                while (1) {
                  cout << "-------CONFIRMATION-------" << endl;
                  cout << "Please confirm with 'now' or 'later' ";
                  cout << "(other input will not be recognized): ";
                  string input = "mortgage";
                  playerList[ownerID]->strategy(input);
                  cin >> input;
                  
                  if (cin.eof()) {
                    cin.clear();
                    continue;
                  }
                  if (input != "now" && input != "later") {
                    continue;
                  }
                }
              } // if not human
                
              if (input == "now") { playerList[ownerID]->pay(price2,0); }
              else if (input == "later") {
                cout << printName << " did not unmortgage ";
                cout << buildingName << endl;
              } // end "later"
            } // end if mortgaged
          } // end loop-ownable
        } // end loop-i->40
        cout << "---------------------" << endl;
        return;
      } // end if confirmed
    } // end "bankrupt"

    else if (input == "improve") {
      bool isValid = false;
      string action;
      cin >> buildingName >> action;
      if (!cin) {
        cin.clear();
        cin.ignore();
        cout << "Invalid input. Please try again" << endl;
        continue;
      }

      // check if valid input for the property
      for (int i=0; i<40; i++) {
        if (isOwnable[i] && buildingName == buildingList[i]->getName()) {
          if (isAcademic[i]) {
            isValid = true;
            buildingID = i;
          }
        }
      }
      if (!isValid) {
        cout << buildingName << " is not an Academic building" << endl;
        cout << "please try again" << endl;
        continue;
      }

      if (action == "sell") {
        changeImprovement(buildingID,'-');
        pay(amount,ownerID);
        break;
      } else if (action == "buy") {
        cout << "You are limited to only sell improvements" << endl;
        continue;
      } else {
        cout << "Invalid input. Please try again" << endl;
        continue;
      }
    } // end if improve

    else if (input == "mortgage") {
      bool isValid = false;
      cin >> buildingName;
      if (!cin) {
        cin.clear();
        cin.ignore();
        cout << "Invalid input. Please try again" << endl;
        continue;
      }

      // check if valid input for the property
      for (int i=0; i<40; i++) {
        if (isOwnable[i] && buildingName == buildingList[i]->getName()) {
          isValid = true;
          buildingID = i;
        }
      }
      if (!isValid) {
        cout << buildingName << " is not an Owanble building" << endl;
        cout << "please try again" << endl;
        continue;
      }

      changeMortgage(buildingID,'+');
      pay(amount,ownerID);
      break;
    } // end mortgage

    // other input
    else {
      cout << "You are limited to use the following command: " << endl;
      cout << "command: improve <property> sell" << endl;
      cout << "command: mortgage <property>" << endl;
      cout << "command: bankrupt" << endl;
      continue;
    }
  } // end while-loop
}

bool Human::getBankruptcy() { return isBankrupt; }

void Human::setBankruptcy(bool bankrupt) { isBankrupt = bankrupt; }

void Human::strategy(string &decision) {}

