//
//  ev3.h
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#ifndef __EV3_H__
#define __EV3_H__

#include <iostream>
#include <string>
#include <map>
#include "env.h"

class EV3:public ENV{
public:
    EV3();
    int getPurchaseCost();
    int getImprovementCost();
    void changeImprovement(std::string act);
    int getImprovement();
    bool checkMonopoly(bool whetherPrint);
    bool checkMonopoly();
    int getFees();
    int getFees(std::string);
    std::string getName();
    std::string getOwner();
    void setOwner(std::string name);
    bool getMortgage();
    void setMortgage(bool whetherMortgage);
    ~EV3();
};
#endif