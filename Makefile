CXX = g++
CXXFLAGS = -Wall -MMD
EXEC = bb7k
OBJECTS = main.o academic.o al.o arts1.o arts2.o b1.o b2.o bank.o bmh.o board.o building.o c2.o cif.o clv.o cph.o dc.o dwe.o easycomputer.o ech.o eit.o eng.o env.o esc.o ev1.o ev2.o ev3.o gym.o health.o hh.o human.o lhi.o math.o mc.o mkv.o ml.o normalcomputer.o opt.o pac.o pas.o phys.o player.o rch.o residence.o rev.o sci1.o sci2.o uwp.o v1.o
DEPENDS = ${OBJECTS:.o=.d}

${EXEC}: ${OBJECTS}
	${CXX} ${CXXFLAGS} ${OBJECTS} -o ${EXEC}

-include ${DEPENDS}

.PHONY: clean

clean:
	rm ${OBJECTS} ${EXEC} ${DEPENDS}
