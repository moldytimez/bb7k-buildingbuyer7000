//
//  b2.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "b2.h"
#include "sci1.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

B2::B2():SCI1(150,280,"B2"){
    tuitionList[0] = 24;
    tuitionList[1] = 120;
    tuitionList[2] = 360;
    tuitionList[3] = 850;
    tuitionList[4] = 1025;
    tuitionList[5] = 1200;
}


int B2::getPurchaseCost(){
    return purchaseCost;
}

int B2::getImprovementCost(){
    return improvementCost;
}

void B2::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["B2"] = improvement;
}


int B2::getImprovement(){
    return improvement;
}

bool B2::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["PHYS"] == owner && ownerList["B1"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["PHYS"] == owner && ownerList["B1"] != owner)
            cout << "The property required to be owned is B1 (owner: " << ownerList["B1"] << ")" << endl;
        if(ownerList["PHYS"] != owner && ownerList["B1"] == owner)
            cout << "The property required to be owned is PHYS (owner: " << ownerList["PHYS"] << ")" << endl;
        if(ownerList["PHYS"] != owner && ownerList["B1"] != owner){
            cout << "The property required to be owned is PHYS (owner: " << ownerList["PHYS"] << ")" << endl;
            cout << "The property required to be owned is B1 (owner: " << ownerList["B1"] << ")" << endl;
        }
    }
    return isMonopoly;
}


bool B2::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["PHYS"] == owner && ownerList["B1"]== owner) isMonopoly = true;
    return isMonopoly;
}


int B2::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["PHYS"] == 0 && improvementList["B1"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int B2::getFees(string ownerPiece){return 0;}

string B2::getName(){
    return name;
}

string B2::getOwner(){
    return owner;
}

void B2::setOwner(string name){
    owner = name;
    ownerList["B2"] = name;
}


bool B2::getMortgage(){
    return isMortgage;
}

void B2::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

B2::~B2(){}



