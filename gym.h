//
//  gym.h
//  bb7k
//
//  Created by jessica on 14/11/21.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#ifndef __GYM_H__
#define __GYM_H__

#include <iostream>
#include <string>
#include "building.h"

class GYM:public Building{
protected:
    int usageFee;
public:
    GYM(std::string name);
    int giveDice();
    ~GYM();
};

#endif