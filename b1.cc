//
//  b1.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "b1.h"
#include "sci1.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

B1::B1():SCI1(150,260,"B1"){
    tuitionList[0] = 22;
    tuitionList[1] = 110;
    tuitionList[2] = 330;
    tuitionList[3] = 800;
    tuitionList[4] = 975;
    tuitionList[5] = 1150;
}


int B1::getPurchaseCost(){
    return purchaseCost;
}

int B1::getImprovementCost(){
    return improvementCost;
}

void B1::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["B1"] = improvement;
}


int B1::getImprovement(){
    return improvement;
}

bool B1::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["PHYS"] == owner && ownerList["B2"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["PHYS"] == owner && ownerList["B2"] != owner)
            cout << "The property required to be owned is B2 (owner: " << ownerList["B2"] << ")" << endl;
        if(ownerList["PHYS"] != owner && ownerList["B2"] == owner)
            cout << "The property required to be owned is PHYS (owner: " << ownerList["PHYS"] << ")" << endl;
        if(ownerList["PHYS"] != owner && ownerList["B2"] != owner){
            cout << "The property required to be owned is PHYS (owner: " << ownerList["PHYS"] << ")" << endl;
            cout << "The property required to be owned is B2 (owner: " << ownerList["B2"] << ")" << endl;
        }
    }
    return isMonopoly;
}


bool B1::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["PHYS"] == owner && ownerList["B2"]== owner) isMonopoly = true;
    return isMonopoly;
}


int B1::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["PHYS"] == 0 && improvementList["B2"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int B1::getFees(string ownerPiece){return 0;}

string B1::getName(){
    return name;
}

string B1::getOwner(){
    return owner;
}

void B1::setOwner(string name){
    owner = name;
    ownerList["B1"] = name;
}


bool B1::getMortgage(){
    return isMortgage;
}

void B1::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}


B1::~B1(){}
