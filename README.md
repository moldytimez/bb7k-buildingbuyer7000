# README #

This is a Monopoly-like command line game.

### Compile ###

Use 'make' to compile and 'make clean' to clean builds. 

### Run ###

Run with ./bb7k

### Command Help ###

Type help if you do not know what command you should input

### More Info ###

Please Download the design.pdf to see more information about this game.