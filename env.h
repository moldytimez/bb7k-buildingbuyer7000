//
//  env.h
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#ifndef __ENV_H__
#define __ENV_H__

#include <map>
#include <string>
#include "academic.h"

class ENV:public Academic{
protected:
    static std::map<std::string,std::string> ownerList;
    static std::map<std::string,int> improvementList;
public:
    ENV(int improveCost,int purchaseCost,std::string name);
    ~ENV();
};

#endif
