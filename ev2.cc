//
//  ev2.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "ev2.h"
#include "env.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

EV2::EV2():ENV(150,220,"EV2"){
    tuitionList[0] = 18;
    tuitionList[1] = 90;
    tuitionList[2] = 250;
    tuitionList[3] = 700;
    tuitionList[4] = 875;
    tuitionList[5] = 1050;
}


int EV2::getPurchaseCost(){
    return purchaseCost;
}

int EV2::getImprovementCost(){
    return improvementCost;
}

void EV2::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["EV2"] = improvement;
}


int EV2::getImprovement(){
    return improvement;
}

bool EV2::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["EV1"] == owner && ownerList["EV3"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["EV1"] == owner && ownerList["EV3"] != owner)
            cout << "The property required to be owned is EV3 (owner: " << ownerList["EV3"] << ")" << endl;
        if(ownerList["EV1"] != owner && ownerList["EV3"] == owner)
            cout << "The property required to be owned is EV1 (owner: " << ownerList["EV1"] << ")" << endl;
        if(ownerList["EV1"] != owner && ownerList["EV3"] != owner){
            cout << "The property required to be owned is EV1 (owner: " << ownerList["EV1"] << ")" << endl;
            cout << "The property required to be owned is EV3 (owner: " << ownerList["EV3"] << ")" << endl;
        }
    }
    return isMonopoly;
}


bool EV2::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["EV1"] == owner && ownerList["EV3"]== owner) isMonopoly = true;
    return isMonopoly;
}


int EV2::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["EV1"] == 0 && improvementList["EV3"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}


int EV2::getFees(string ownerPiece){return 0;}

string EV2::getName(){
    return name;
}

string EV2::getOwner(){
    return owner;
}

void EV2::setOwner(string name){
    owner = name;
    ownerList["EV2"] = name;
}


bool EV2::getMortgage(){
    return isMortgage;
}

void EV2::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

EV2::~EV2(){}
