//
//  cph.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "cph.h"
#include "eng.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

CPH::CPH():ENG(100,160,"CPH"){
    tuitionList[0] = 12;
    tuitionList[1] = 60;
    tuitionList[2] = 180;
    tuitionList[3] = 500;
    tuitionList[4] = 700;
    tuitionList[5] = 900;
}


int CPH::getPurchaseCost(){
    return purchaseCost;
}

int CPH::getImprovementCost(){
    return improvementCost;
}

void CPH::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["CPH"] = improvement;
}


int CPH::getImprovement(){
    return improvement;
}

bool CPH::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["RCH"] == owner && ownerList["DWE"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["RCH"] == owner && ownerList["DWE"] != owner)
            cout << "The property required to be owned is DWE (owner: " << ownerList["DWE"] << ")" << endl;
        if(ownerList["RCH"] != owner && ownerList["DWE"] == owner)
            cout << "The property required to be owned is RCH (owner: " << ownerList["RCH"] << ")" << endl;
        if(ownerList["RCH"] != owner && ownerList["DWE"] != owner){
            cout << "The property required to be owned is RCH (owner: " << ownerList["RCH"] << ")" << endl;
            cout << "The property required to be owned is DWE (owner: " << ownerList["DWE"] << ")" << endl;
        }
    }
    return isMonopoly;
}


bool CPH::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["RCH"] == owner && ownerList["DWE"]== owner) isMonopoly = true;
    return isMonopoly;
}


int CPH::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["RCH"] == 0 && improvementList["DWE"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}


int CPH::getFees(string ownerPiece){return 0;}

string CPH::getName(){
    return name;
}

string CPH::getOwner(){
    return owner;
}

void CPH::setOwner(string name){
    owner = name;
    ownerList["CPH"] = name;
}


bool CPH::getMortgage(){
    return isMortgage;
}

void CPH::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

CPH::~CPH(){}