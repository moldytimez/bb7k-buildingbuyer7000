#include "player.h"
#include "normalcomputer.h"
#include "building.h"
#include <string>
#include <iostream>
#include <sstream>
using namespace std;

NormalComputer::NormalComputer(int playerID,string name,string piece,int savings, int position,int rurc, Building **buildingList):Player(playerID,name,piece,savings,position,rurc,buildingList) {
  for (int i=0; i<40; i++) { rejected[i] = ""; }
}

NormalComputer::~NormalComputer() {}

int NormalComputer::getID() { return playerID; }

string NormalComputer::getName() { return name; }

string NormalComputer::getPiece() { return piece; }

int NormalComputer::getSavings() { return savings; }

void NormalComputer::setSavings(int s) { savings = s; }

int NormalComputer::getPosition() { return position; }

void NormalComputer::setPosition(int buildingID) { position = buildingID; }

void NormalComputer::changePosition(int change) {
  position += change;
  if (position < 0) { position += 40; }
  if (position >= 40) { position -= 40; }
  cout << piece << " (" << name << ") have moved to ";
  if (isOwnable[position]) { cout << buildingList[position]->getName(); }
  else if (position==0) { cout << "COLLECT OSAP"; }
  else if (position==2||position==17||position==33) { cout << "SLC"; }
  else if (position==4) { cout << "TUITION"; }
  else if (position==7||position==22||position==36) { cout << "NEEDLES HALL"; }
  else if (position==10) { cout << "DC Tims Line"; }
  else if (position==20) { cout << "Goose Nesting"; }
  else if (position==30) { cout << "GO TO TIMS"; }
  else if (position==38) { cout << "COOP FEE"; }
  
  cout << " by moving " << change << " squares" << endl;
}

int NormalComputer::getDCStatus() { return dcStatus; }

void NormalComputer::setDCStatus(int value) { dcStatus = value; }

int NormalComputer::getRurc() { return rurc; }

void NormalComputer::changeRurc(int change) { rurc+=change; }

void NormalComputer::changeSavings(int change) {
  savings += change;
  cout << piece << " (" << name << ") ";
  if (change > 0) { cout << "got $" << change << " "; }
  else { cout << "paid $" << -change << " "; }
}

void NormalComputer::pay(int amount, int ownerID) {
  string ownerPiece = playerList[ownerID]->getPiece();
  string ownerName = playerList[ownerID]->getName();
  string printName = ownerPiece + " (" + ownerName + ")";
  if (savings > amount) {
    cout << "-------INFORMATION-------" << endl;
    changeSavings(-amount);
    cout << "to " << printName << endl;
    playerList[ownerID]->changeSavings(amount);
    cout << "from " << piece << " (" << name << ")" << endl;
    cout << "---------------------" << endl << endl;
  } else {
    bankruptProcess(amount,ownerID);
  }
}

void NormalComputer::buy(int buildingID) {
  string buildingName = buildingList[buildingID]->getName();
  string ownerPiece = buildingList[buildingID]->getName();
  string ownerName = nameList[ownerPiece];
  int price = buildingList[buildingID]->getPurchaseCost();
  bool isMortgaged = buildingList[buildingID]->getMortgage();

  if (!isMortgaged && savings > price) {
    cout << "-------INFORMATION-------" << endl;
    buildingList[buildingID]->setOwner(piece);
    changeSavings(-price);
    cout << "to buy " << buildingName << endl;
    cout << buildingName << " is now owned by ";
    cout << piece << " (" << name << ")" << endl << endl;
  }
}

void NormalComputer::changeImprovement(int buildingID,char action) {
  string buildingName = buildingList[buildingID]->getName();
  string ownerPiece = buildingList[buildingID]->getOwner();
  string ownerName = nameList[ownerPiece];
  int price = buildingList[buildingID]->getImprovementCost();
  int improvement = buildingList[buildingID]->getImprovement();
  bool isMortgaged = buildingList[buildingID]->getMortgage();
  bool isMonopoly = buildingList[buildingID]->checkMonopoly();

  // if buy improvement
  if (action == '+') {
    if (!isMortgaged && savings > price && improvement != 5 && isMonopoly) {
      cout << "-------INFORMATION-------" << endl;
      changeSavings(-price);
      cout << "to upgrade " << buildingName << endl;
      buildingList[buildingID]->changeImprovement("in");
      cout << buildingName << " is now of level " << improvement+1 << endl;
      cout << "---------------------" << endl;
    }
  
  // if sell improvement
  } else if (action == '-') {
    if (!isMortgaged && improvement != 0) {
      cout << "-------INFORMATION-------" << endl;
      changeSavings(price/2);
      cout << "by degrading " << buildingName << endl;
      buildingList[buildingID]->changeImprovement("de");
      cout << buildingName << " is now of level " << improvement-1 << endl;
      cout << "---------------------" << endl << endl;
    }
  }
}

void NormalComputer::changeMortgage(int buildingID,char action) {
  string buildingName = buildingList[buildingID]->getName();
  string ownerPiece = buildingList[buildingID]->getOwner();
  string ownerName = nameList[ownerPiece];
  int price = buildingList[buildingID]->getPurchaseCost()/2;
  int improvement = buildingList[buildingID]->getImprovement();
  bool isMortgaged = buildingList[buildingID]->getMortgage();

  // if mortgage
  if (action == '+') {
    if (!isMortgaged && improvement == 0) {
      cout << "-------INFORMATION-------" << endl;
      changeSavings(price);
      cout << "by mortgaging " << buildingName << endl;
      buildingList[buildingID]->setMortgage(true);
      cout << buildingName << " is mortgaged" << endl;
    }

  // if unmortgage
  } else if (action == '-') {
    if (isMortgaged && savings >= price*1.1) {
      changeSavings(-(price*1.1));
      cout << "to unmortgage " << buildingName << endl;
      buildingList[buildingID]->setMortgage(false);
      cout << piece << " (" << name << ") paid $" << price*1.1 << " to unmortgage";
      cout << buildingName << endl;
      cout << buildingName << " is unmortgaged" << endl ;
    }
  }
}

void NormalComputer::giveAsset() {}

void NormalComputer::bankruptProcess(int amount, int ownerID) {
  stringstream ss;
  ss << amount;
  string decision = ss.str();
  strategy(decision);

  // if declared bankruupt
  if (decision == "bankrupt") {
    cout << piece << "(" << name << ") declared bankruptcy" << endl;
    setBankruptcy(true);
    // change owner
    string ownerPiece = playerList[ownerID]->getPiece();
    string ownerName = playerList[ownerID]->getName();
    string printName = ownerPiece + " (" + ownerName + ")";
    string input = "";
    
    for (int i=0; i<40; i++) {
      if (isOwnable[i] && buildingList[i]->getOwner() == piece) {
        string buildingName = buildingList[i]->getName();
        buildingList[i]->setOwner(ownerPiece);
        cout << "-------INFORMATION-------" << endl;
        cout << buildingName << " is now owned by " << printName << endl;
        
        // for building mortgaged and the fees is not paid to bank
        if (buildingList[i]->getMortgage() && ownerID != 0) {
          cout << "Since " << buildingName << " is mortgaged, ";
          cout << printName << " must pay 10% of its price to the BANK: ";
          int cost = buildingList[i]->getPurchaseCost();
          int price1 = 0.1*cost;
          cout << "$" << price1 << endl;
          playerList[ownerID]->pay(price1,0);
          
          string decision = "mortgage";
          playerList[ownerID]->strategy(decision);
          int price2 = cost/2;
          int price3 = price2*1.1;
          if (decision == "mortgage") {
            cout << "-------SELECTION-------" << endl;
            cout << "You may choose to unmortgage the property right now ";
            cout << "by paying $" << price2 << endl;
            cout << "OR you may choose to unmortgage the property later ";
            cout << "by paying $" << price3 << " at that time" << endl;
            while (1) {
              cout << "-------CONFIRMATION-------" << endl;
              cout << "Please confirm with 'now' or 'later' ";
              cout << "(other input will not be recognized): ";
              string input = "mortgage";
              playerList[ownerID]->strategy(input);
              cin >> input;
              
              if (cin.eof()) {
                cin.clear();
                continue;
              }
              if (input != "now" && input != "later") {
                continue;
              }          
              if (input == "now") {
                playerList[ownerID]->pay(price2,0);
                break; 
              } else if (input == "later") {
                cout << printName << " did not unmortgage " << buildingName << endl;
                break;
              }
            }
          }
        }
      }
    }
    cout << "---------------------" << endl << endl;
    return;
  } // end "declare"
  else { pay(amount,ownerID); }
} // end bankruptProcess


bool NormalComputer::getBankruptcy() { return isBankrupt; }

void NormalComputer::setBankruptcy(bool bankrupt) { isBankrupt = bankrupt; }


void NormalComputer::strategy(string &decision) {
  // if other player declared bankruptcy and owned owner is current COM
  if (decision == "bankruptMortgage") {
    if (savings > 1000) { decision = "now"; }
    else { decision = "later"; }
    return;
  }
  
  // get required info for easier following process
  bool ownedList[40] = {false};     // true if owned property,false otherwise
  bool ownedAcaList[40] = {false};  // true if owned academic block,false otherwise
  bool mortList[40] = {false};    // true if owned mortgaged bloack,false otherwise
  string monopoly[40] = {""};       // index as ID, value as Monopoly Block
  bool ownedMonoList[40] = {false}; // true if this building is in Monopoly

  for (int i=0; i<40; i++) {
    if (isOwnable[i]) {
      int cost = buildingList[i]->getPurchaseCost();
      if (isAcademic[i]) {
        int impr = buildingList[i]->getImprovement();
        int imprCost = buildingList[i]->getImprovementCost();
        if (buildingList[i]->getOwner() == piece) { ownedAcaList[i] = true; }
        originalV[i] = cost+impr*imprCost;
      } else {
        if (buildingList[i]->getOwner() == piece) { ownedList[i] = true; }
        originalV[i] = cost;
      }
    } else { ownedList[i] = false; }
    
    if (isOwnable[i] && buildingList[i]->getMortgage()) { mortList[i] = true; }
    else { mortList[i] = false; }
  }
  
  monopoly[1] = monopoly[3] = "A1";
  monopoly[6] = monopoly[8] = monopoly[9] = "A2";
  monopoly[11] = monopoly[13] = monopoly[14] = "ENG";
  monopoly[16] = monopoly[18] = monopoly[19] = "H";
  monopoly[21] = monopoly[23] = monopoly[24] = "ENV";
  monopoly[26] = monopoly[27] = monopoly[29] = "S1";
  monopoly[31] = monopoly[32] = monopoly[34] = "S2";
  monopoly[37] = monopoly[39] = "M";
  
  for (int i=0; i<=37; i++) {
    for (int j=i+1; j<=39; j++) {
      if (ownedList[i] && ownedList[j] && monopoly[i] == monopoly[j]) {
        ownedMonoList[i] = true;
        ownedMonoList[j] = true;
      }
    }
  }

  if (decision != "action" && decision != "") {
  
  stringstream ss1(decision);
  stringstream ss2(decision);

  // if trade is requested
  int oppoID;
  string give, receive;
  int amount;
  if (ss1 >> oppoID && ss1 >> give && ss1 >> receive) {
    int money;
    stringstream giveIsMoney(give);
    stringstream receiveIsMoney(receive);
    
    if (giveIsMoney >> money) {
      for (int i=0; i<40; i++) {
        if (isOwnable[i] && receive == buildingList[i]->getName()) {
          int impr = buildingList[i]->getImprovement();
          int value = buildingList[i]->getPurchaseCost();
          value += impr*buildingList[i]->getImprovementCost();
          if (i <=37) {
            for (int j=i+1; j<=39; j++) {
              if (ownedList[j] && monopoly[i] == monopoly[j]) { value = value*2; }
            }
          }

          // if not much savings or relatively poor price
          if (savings < 500 ||
              (savings < 1000 && savings > money+500 &&
               (double)money*1.3 >= value) ||
              (savings > money+800 && (double)money*1.5 >= value)) {
            decision = "no";
          } else { decision = "yes"; }
          return;
        }
      }
    } // if give is money

    else if (receiveIsMoney >> money) {
      for (int i=0; i<40; i++) {
        if (isOwnable[i] && give == buildingList[i]->getName()) {
          int impr = buildingList[i]->getImprovement();
          int value = buildingList[i]->getPurchaseCost();
          value += impr*buildingList[i]->getImprovementCost();
          if (ownedMonoList[i]) { value = value*3; }
          // if not much savings or not so important building
          if ((savings < 500 && value <= money) || (value*2 <= money)) {
            decision = "yes";
          } else { decision = "no"; }
          return;
        }
      }
    } // end if receive is money

    else {
      int giveID,receiveID,valueG,valueR;
      for (int i=0; i<40; i++) {
        if(isOwnable[i] && give == buildingList[i]->getName()) { giveID = i; }
        if (isOwnable[i] && receive == buildingList[i]->getName()) {
          receiveID = i;
        }
      }
      valueG = buildingList[giveID]->getPurchaseCost();
      valueR = buildingList[receiveID]->getPurchaseCost();
      int imprG = buildingList[giveID]->getImprovement();
      int imprR = buildingList[receiveID]->getImprovement();
      valueG += imprG*buildingList[giveID]->getImprovementCost();
      valueR += imprR*buildingList[receiveID]->getImprovementCost();
      if (ownedMonoList[giveID]) { valueG = valueG*3; }
      for (int i=0; i<40; i++) {
        if (ownedList[i] && monopoly[i] == monopoly[receiveID]) {
            valueR = valueR*3;
        }
      }

      if (valueG*2 <= valueR) { decision = "yes"; }
      else { decision = "no"; }
      return;
    } // end if both buildings
  } // end if trade

  // if bankruptProcess is called
  else if (ss2 >> amount) {

    // check if need to declare bankrupt
    int money = savings;
    for (int i=0; i<40; i++) {
      if (ownedList[i] && !mortList[i]) {
        money += buildingList[i]->getPurchaseCost()/2;
        if (ownedAcaList[i]) {
          int impr = buildingList[i]->getImprovement();
          money += impr*buildingList[i]->getImprovementCost()/2;
        }
      }
    }

    // if money being able to get < amount to pay, declare bankrupt
    if (money < amount) {
      decision = "bankrupt";
      return;
    }

    // if moeny being able to get >=  amount to pay
    else {
      // if possible, prefer selling improvements
      for (int i=5; i>0; i--) {           // prefer selling from high level
        for (int j=39; j>=0; j--) {       // prefer selling from expensive
          if (ownedAcaList[j] && !mortList[j] &&
              i == buildingList[j]->getImprovement()) {
            changeImprovement(j,'-');
            if (savings >= amount) { return; }
          }
        } // end selling from expensive
      } // end selling from high level
      
      // if possible, prefer mortgaging non-monopoly
      for (int i=39; i>=3; i--) {
        if (ownedList[i] && !ownedMonoList[i] && !mortList[i]) {
          changeMortgage(i,'+');
          mortList[i] = true;
          if (savings >= amount) { return; }
        }
      } // end mortgaging non-monopoly

      // otherwise, mortgaging the rest
      for (int i=39; i>=1; i--) {
        if (ownedList[i] && !mortList[i]) {
          changeMortgage(i,'+');
          mortList[i] = true;
          if (savings >= amount) { return; }
        }
      } // end mortgaging the rest
      
    } // end if money >= amount
    
  }  // end processing bankrupt

  
  } // end if decision != ""
    
  
  // decision of buying the property or not
  if (isOwnable[position] && buildingList[position]->getOwner() == "BANK") {
    int price = buildingList[position]->getPurchaseCost();
    if (savings > price+600) {
      buy(position);
    }
  }

  if (decision == "action") {
  // decision of improving the property or not
  for (int i=0; i<10; i++) {
    if (ownedAcaList[i] && savings > 700) { changeImprovement(i,'+'); }
  }
  for (int i=10; i<20; i++) {
    if (ownedAcaList[i] && savings > 800) { changeImprovement(i,'+'); }
  }
  for (int i=20; i<30; i++) {
    if (ownedAcaList[i] && savings > 1000) { changeImprovement(i,'+'); }
  }
  for (int i=30; i<40; i++) {
    if (ownedAcaList[i] && savings > 1500) { changeImprovement(i,'+'); }
  }
  
  // decision of unmortgaging a building or not
  for (int i=0; i<10; i++) {
    if (mortList[i] && savings > 1000) { changeMortgage(i,'-'); }
  }
  for (int i=10; i<20; i++) {
    if (mortList[i] && savings > 1200) { changeMortgage(i,'-'); }
  }
  for (int i=20; i<30; i++) {
    if (mortList[i] && savings > 1400) { changeMortgage(i,'-'); }
  }
  for (int i=30; i<40; i++) {
    if (mortList[i] && savings > 2000) { changeMortgage(i,'-'); }
  }

  } // end decision == "action"
  
  // if non-property building
  // if at SLC
  if (position == 4 && decision != "done" && decision != "action") {
    int totalWorth = savings;
    for (int i=0; i<40; i++) {
      if (ownedList[i]) {
        totalWorth += buildingList[i]->getPurchaseCost();
        if (ownedAcaList[i]) {
          int improvement = buildingList[i]->getImprovement();
          totalWorth += buildingList[i]->getImprovementCost()*improvement;
        }
      }
    }
    if (totalWorth*0.1 > 300) { decision = "amount"; }
    else { decision = "percentage"; }

    // ac DC Tims Line
  } else if ((position == 30 || position == 10) &&
             decision != "done" && decision != "action") {
    if (rurc != 0) { decision = "rimcup"; }
    else { decision = "dice"; }
  }
  /*
  if (decision == "action") {
  
  cout << "cp1" << endl;
  // if request trade
  if (savings > 1500) {
    string receiveB = "";
    string oppoPiece;
    stringstream giveSS;
    for (int i=0; i<40; i++) {
      //request to buy for monopoly
      if (ownedAcaList[i] && !ownedMonoList[i]) {
        for (int j=i+1; j<=39; j++) {
          if (monopoly[i] == monopoly[j]) {
            cout << originalV[j] << endl;
            oppoPiece = buildingList[j]->getOwner();
            if (oppoPiece == "BANK" || oppoPiece == piece) { break; }
            if (rejected[j] == oppoPiece) { break; }

            receiveB = buildingList[j]->getName();
 
            cout << oppoPiece << " " << rejected[j] << endl;
            // if first time requesting buying the building to oppoPiece
            if (rejected[j] == "" || rejected[j] != oppoPiece) {
              if (originalV[j]-200 <=0 ) { giveSS << originalV[j]; }
              else { giveSS << originalV[j]+200; }
              break;
            } else { continue; }
          }
        }
      }
    }
    if (receiveB != "" && oppoPiece != "BANK") {
      decision = oppoPiece + " " + giveSS.str() + " " + receiveB;
    }
  } // end buy
  cout << "cp2" << endl; 
  // if no money, have to sell
  if (savings < 500) {
    string giveB = "";
    string oppoPiece;
    stringstream receiveSS;
    int highSavings = 0;
    cout << "cp2.00" << endl;
    for (int j=1; j<7; j++) {
      cout << playerList[j];
      if (playerList[j] != NULL) {
        cout << j << endl;
        cout << "cp2.01"<<endl;
        if(j!=playerID) {
          cout<<"cp2.02"<<endl;
          if (playerList[j]->getSavings() > highSavings) {
            oppoPiece = playerList[j]->getPiece();
          }
        }
      }
    }
    cout <<"cp2.0"<<endl;
    for (int i=39; i>=1; i--) {
      if (ownedList[i]) {
        cout<<"cp2.1"<<endl;
        
        cout<<"cp2.2"<<endl;
        giveB = buildingList[i]->getName();
        cout<<"cp2.3"<<endl;
        if (ownedAcaList[i]) {
          if (ownedMonoList[i]) {
            if (rejected[i] == oppoPiece) {
              continue;
            } else {
              receiveSS << originalV[i]+500; }
            break;
          } else {
            if (rejected[i] == oppoPiece) {
              continue;
            } else { receiveSS << originalV[i]+200; }
            break;
          }
          break;
        } else {
          if (rejected[i] == oppoPiece) {
            continue;
          } else { receiveSS << originalV[i]+300; }
          break;
        }
      } else { giveB = ""; }
      cout<<"cp2.4"<<endl;
    } // end for-40
    if (giveB != "") {
      decision = oppoPiece + " " + giveB + " " + receiveSS.str();
    }
  } // end sell
  cout << "cp3" << endl;

  } // end decision == "action"*/
} // end strategy
