//
//  gym.cpp
//  bb7k
//
//  Created by jessica on 14/11/21.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "gym.h"
#include "building.h"
#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
using namespace std;

GYM::GYM(string name):Building(150,name),usageFee(0){}

int GYM::giveDice(){
    static bool called = false;
    if(!called) {
        srand(time(NULL));
        called = true;
    }
    return rand()%6+1;
}

GYM::~GYM(){}