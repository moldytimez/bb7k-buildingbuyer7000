#include "board.h"
#include "player.h"
#include "building.h"
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>
#include <cstdlib>
using namespace std;

void printHelp() {
  cout << "-------HELP-------" << endl;
  cout << "command: roll" << endl;
  cout << "         (roll two dice, move the sum of the two dice)" << endl;
  cout << "command: next" << endl;
  cout << "         (give control to the next player)" << endl;
  cout << "command: trade <name> <give> <receive>" << endl;
  cout << "         (offers a trade to <name> with the current player offering <give> and requesting <receive>, <give> and <receive> are either amounts of money or a property name)" << endl;
  cout << "command: improve <property> buy" << endl;
  cout << "         (attempts to buy a improve for <property>)" << endl;
  cout << "command: improve <property> sell" << endl;
  cout << "         (attempts to sell a improve for <property>)" << endl;
  cout << "command: mortgage <property>" << endl;
  cout << "         (attempts to mortgage <property>)" << endl;
  cout << "command: unmortgage <property>" << endl;
  cout << "         (attempts to unmortgage <property>)" << endl;
  cout << "command: bankrupt" << endl;
  cout << "         (declares bankruptcy)" << endl;
  cout << "command: assets" << endl;
  cout << "         (displays the assets of the current player)" << endl;
  cout << "command: info <property>" << endl;
  cout << "         (displays the information for the <property>)" << endl;
  cout << "command: save <filename>" << endl;
  cout << "         (saves the current state of the game to <filename>)" << endl;
  cout << "command: quit" << endl;
  cout << "         (ends the game)" << endl;
  cout << "command: help" << endl;
  cout << "         (displays the above text)" << endl;
  cout << "-------HELP-------" << endl;
}

void usage() {
  cout << "Available Arguments: " << endl;
  cout << "bb7k -testing" << endl;
  cout << "bb7k -load <filename>" << endl;
  cout << "bb7k -testing -load <filename>" << endl;
  cout << "bb7k -load <filename> -testing" << endl;
}

int main(int argc, char *argv[]) {
  bool isTesting = false;
  bool loadGame = false;
  string input;
  ifstream f;
  if (argc == 2) {
    input = argv[1];
    if (input == "-testing") { isTesting = true; }
    else {
      usage();
      return 0;
    }
  } else if (argc == 3) {
    input = argv[1];
    if (input == "-load") {
      input = argv[2];
      f.open(input.c_str());
      if (!f) {
        cout << "file " << input << "does not exist" << endl;
        return 0;
      }
      loadGame = true;
    } else {
      usage();
      return 0;
    }
  } else if (argc == 4) {
    input = argv[1];
    if (input == "-load") {
      input = argv[2];
      f.open(input.c_str());
      if (!f) {
        cout << "file " << input << "does not exist" << endl;
        return 0;
      }
      loadGame = true;
      input = argv[3];
      if (input == "-testing") { isTesting = true; }
      else {
        usage();
        return 0;
      }
    } else if (input == "-testing") {
      isTesting = true;
      input = argv[2];
      if (input == "-load") {
        input = argv[3];
        f.open(input.c_str());
        if (!f) {
          cout << "file " << input << "does not exist" << endl;
          return 0;
        }
        loadGame = true;
      } else {
        usage();
        return 0;
      }
    } else {
      usage();
      return 0;
    }
  } else if (argc != 1) {
    usage();
    return 0;
  }

  int numPlayer;
  int actualNumPlayer; // will decrease if some player declared bankrupt
  
  Board b;
  // default 1; decrease by 1 if rolled once; increase by 1 if rolled doubles
  int rollNum = 1;
  // if rollCount == 3, current player is sent to DC Tims Line
  int rollCount = 0;
  // if current player in dc has already selected the choice
  bool dcChecked = false;
  
  // if start from loading a file
  if (loadGame) {

    f >> numPlayer >> actualNumPlayer;

    string give500;
    f >> give500;
    if (give500 == "true") b.give500 = true;
    else if (give500 == "false") b.give500 = false;
    else cout << "WRONG" << endl;
    
    // add player
    int rurcCount = 0;
    for (int i=1; i<actualNumPlayer+1; i++) {
      string firstName, lastName, piece;
      int savings, position, rurc;
      f >> firstName >> lastName >> piece >> savings >> rurc >> position;
      rurcCount += rurc;
      stringstream oss;
      oss << firstName << " " << lastName;
      string name = oss.str();
      b.addPlayer(i,name,piece,savings,position,rurc);
      if (position == 10) {
        int status;
        f >> status;
        if (status == 0) {
          b.playerList[i]->setDCStatus(-1);
        } else if (status == 1) {
          string dcCheckedStr;
          f >> status >> dcCheckedStr;
          b.playerList[i]->setDCStatus(status);
          if (dcCheckedStr == "true") { dcChecked = true; }
          else { dcChecked = false; }
        }
      }
      if (i == 1) {
        f >> rollNum >> rollCount;
      }
    }
    b.addPlayerList();
    b.playerList[0]->rurcCount = rurcCount;
   
    // update building status
    for (int i=0; i<40; i++) {
      if (b.isOwnable[i]) {
        string owner,isMortgaged;
        int improvement;
        f >> input >> owner;

        b.buildingList[i]->setOwner(owner);
        if (b.isAcademic[i]) {
          f >> improvement;
          for (int j=0; j<improvement; j++) {
            b.buildingList[i]->changeImprovement("in");
          }
        }
        f >> isMortgaged;
        if (isMortgaged == "true") { b.buildingList[i]->setMortgage(true); }
        else { b.buildingList[i]->setMortgage(false); }
      }
    } // for
    f.close();
  }

  // if start new game
  else {

    int COMNum = 0;
    string pieceList[] = {"G","B","D","P","S","$","L","T"};
    map<string,bool> checkPiece; // check if the piece is chosen already
    for (int i=0; i<8; i++) { checkPiece[pieceList[i]] = false; }

    cout << "Computer Players count in the number of players" << endl;
    cout << "At least one Computer Player need to be added to the game" << endl;
    cout << "Please enter the number of players (2~6): ";
    while (1) {
      cin >> numPlayer;
      if (!cin || numPlayer < 2 || numPlayer > 6) {
        cout << "Invalid input. Please input an integer (2~6): ";
        if(cin.eof()) {
          cin.clear();
        }
        else if (cin.fail()) {
          cin.clear();
          cin.ignore();
          continue;
        }
      } else { break; }
    } // end input numPlayer

    // create Player object
    for (int i=1; i<numPlayer+1; i++) {
      string name,piece;
      cout << "-----Player " << i << "-----" << endl;
      if (i == numPlayer && COMNum == 0) {
        cout << "The game must have at least one COM player" << endl;
        cout << "Player " << i << " will be a COM Player" << endl;
        input = "no";
      } else {
        while (1) {
          cout << "Is this a human player ? ('yes' or 'no') ";
          cin >> input;
          if (!cin || (input != "yes" && input != "no")) {
            cout << "Invalid input" << endl;
            if (!cin) { cin.clear(); }
          } else { break; }
        }
      }
      
      // if human player
      if (input == "yes" ) {
        string lastName;
        string firstName;
        cout << "What is your first name? ";
        while (1) {
          cin >> firstName;
          if (!cin) {
            cout << "Invalid input" << endl;
            cin.clear();
          } else { break; }
        }
        while (1) {
          cout << "What is your last name? ";
          cin >> lastName;
          if (!cin) {
            cout << "Invalid input" << endl;
            cin.clear();
          } else { break; }
        }

        stringstream oss;
        oss << firstName << " " << lastName;
        name = oss.str();
        
        cout << "You can choose one piece to represent on the board" << endl;
        cout << "Current available pieces are: " << endl;
        if (checkPiece["G"] == false) { cout << "G(Goose) "; }
        if (checkPiece["B"] == false) { cout << "B(GRT Bus) "; }
        if (checkPiece["D"] == false) { cout << "D(Tim Hortons Doughnut) "; }
        if (checkPiece["P"] == false) { cout << "P(Professor) "; }
        if (checkPiece["S"] == false) { cout << "S(Student) "; }
        if (checkPiece["$"] == false) { cout << "$(Money) "; }
        if (checkPiece["L"] == false) { cout << "L(Laptop) "; }
        if (checkPiece["T"] == false) { cout << "T(Pink tie)"; }
        cout << endl;
        cout << "Please type the piece you want (one character): ";
        cin >> piece;
        while (1) {
          if (!cin) {                   // if EOF
            cout << "Invalid input. Please type the piece you want: ";
            cin.clear();
            cin.ignore();
            cin >> piece;
          } else {
            bool isValid = false;
            for (int i=0; i<8; i++) {   // check if valid input
              if (piece == pieceList[i]) {
                isValid = true;
                break;
              }
            }
            if (!isValid) {
              cout << "Invalid input. Please type the piece name correctly: ";
              cin >> piece;
              continue;
            }
            
            bool isPicked = false;
            for (int i=0; i<8; i++) {
              if (piece == pieceList[i] && checkPiece[piece] == true) {
                cout << piece << " has been chosen" << endl;
                isPicked = true;
                cout << "Current available pieces are: " << endl;
                if (checkPiece["G"] == false) { cout << "G(Goose) "; }
                if (checkPiece["B"] == false) { cout << "B(GRT Bus) "; }
                if (checkPiece["D"] == false) { cout << "D(Tim Hortons Doughnut "; }
                if (checkPiece["P"] == false) { cout << "P(Professor) "; }
                if (checkPiece["S"] == false) { cout << "S(Student) "; }
                if (checkPiece["$"] == false) { cout << "$(Money) "; }
                if (checkPiece["L"] == false) { cout << "L(Laptop) "; }
                if (checkPiece["T"] == false) { cout << "T(Pink tie)"; }
                cin >> piece;
                break;
              }
            }
            if (!isPicked) {
              checkPiece[piece] = true;
              break;
            }
          }
        } // while
        
        // create the Human object
        if (name == "Bruce Wayne") {      // if Bruce Wayne
          b.addPlayer(i,name,piece,15000,0,0);
          b.buildingList[37]->setOwner(piece); // change MC's owner
          b.buildingList[39]->setOwner(piece); // change DC's owner
        } else {
          b.addPlayer(i,name,piece,1500,0,0);
        }
      } // end if Human Player
      
      // if Computer Player
      else if (input == "no") {
        string level;
        cout << "You can choose a level for this Computer Player: e(asy),n(ormal)";
        cout << endl << "Please choose a level for this Computer Player: ";
        cin >> level;
        while (1) {
            stringstream ossLevel(level);
            int savingsNum;
          if (!cin || (level != "e" && level != "n") || (isTesting && ossLevel >> savingsNum && !ossLevel)) {
            cout << "Invalid input. Please type 'e' or 'n': ";
            cin.clear();
            cin >> level;
          } else { break; }
        }

        COMNum++;
        bool isValid = false;

        while (!isValid) {
          srand(time(NULL));
          int pieceID = rand()%8;
          if (checkPiece[pieceList[pieceID]] == false) {
            piece = pieceList[pieceID];
            checkPiece[piece] = true;
            isValid = true;
          }
        }
        
          stringstream ss(level);
          int savingsNum;
        if (level == "e") {
          b.addPlayer(i,"EASY COM",piece,1500,0,0);
        } else if (level == "n") {
          b.addPlayer(i,"NORMAL COM",piece,1500,0,0);
        } else if (isTesting && ss >> savingsNum) {
          b.addPlayer(i,"EASY COM",piece,savingsNum,0,0);
        }
      } // end if Computer Player

      cout << "Player " << i << " have chosen " << piece;
      cout << " to be the piece" << endl << endl;
      
    } // end adding Players
    b.addPlayerList();
    actualNumPlayer = numPlayer;
  } // end if new game

  printHelp(); // print help information for commands
  cout << endl << "-------GAME START-------" << endl << endl;
  b.draw();
  
  // track the current Player's playerID
  int playerID = 1;
  bool isCOM = false;
  string decision = "";                 // used for COM 'input'
  
  // game process
  while (1) {
    if (playerID > numPlayer) { playerID-=numPlayer; }
    // jump the turn of  some player declared bankruptcy
    if (b.playerList[playerID] == NULL) {
      playerID++;
      if (playerID > numPlayer) { playerID-=numPlayer; }
      continue;
    }
    // check if some player declared bankruptcy
    else if (b.playerList[playerID]->getBankruptcy()) {
      actualNumPlayer--;
      delete b.playerList[playerID];
      b.playerList[playerID] = NULL;
      playerID++;
      rollNum = 1;
      rollCount = 0;
      decision = "";
      dcChecked = false;
      continue;
    }

    // process of ending the game
    if (actualNumPlayer == 1) {
      cout << endl << endl;
      cout << "----------CONGRATULATIONS----------" << endl;
      cout << b.playerList[playerID]->getName() << " (";
      cout << b.playerList[playerID]->getPiece() << ") is the Winner!" << endl;
      cout << "-----------------------------------" << endl;
      cout << "Thanks for playing! The program will quit" << endl;
      cout << "-----------------------------------" << endl;
      delete b.playerList[playerID];
      delete b.playerList[0];
      return 0;
    }
     
    string name = b.playerList[playerID]->getName();
    string piece = b.playerList[playerID]->getPiece();
    int savings = b.playerList[playerID]->getSavings();
    int position = b.playerList[playerID]->getPosition();
    string buildingName = "";
    if (b.isOwnable[position]) {
      buildingName = b.buildingList[position]->getName();
    } else if (position == 0) {
      buildingName = "COLLECT OSAP";
    } else if (position == 2 || position == 17 || position == 33) {
      buildingName = "SLC";
    } else if (position == 4) {
      buildingName = "TUITION";
    } else if (position == 7 || position == 22 || position == 36) {
      buildingName = "NEEDLES HALL";
    } else if (position == 10) {
      buildingName = "DC Tims Line";
    } else if (position == 20) {
      buildingName = "Goose Nesting";
    } else if (position == 38) {
      buildingName = "COOP FEE";
    }
    int buildingID = position;

    // show the basic information
    cout << "-------Player " << piece << "'s turn-------" << endl;
    cout << "Name: " << name << "     Piece: " << piece << endl;
    cout << "Savings: " << savings << "     Current at " << buildingName << endl;
    cout << "Roll Up the Rim cup: " << b.playerList[playerID]->getRurc() << endl;

    // if at DC Tims Line
    int dcStatus = b.playerList[playerID]->getDCStatus();
    if (dcStatus != -1 && !dcChecked) {
      b.playerList[playerID]->strategy(decision);
      b.dcTimsLine(playerID,decision);
      decision = "";
      dcChecked = true;
      if (b.playerList[playerID]->getDCStatus() == -1) { rollNum = 0; }
    }
    
    dcStatus = b.playerList[playerID]->getDCStatus();
    
    // if computer player
    if (b.playerList[playerID]->getName() == "EASY COM" ||
        b.playerList[playerID]->getName() == "NORMAL COM") {
      isCOM = true;
      if (b.playerList[playerID]->getBankruptcy()) { continue; }
      if (rollNum != 0 && dcStatus == -1) { input = "roll"; }
      else {
        decision = "action";
        b.playerList[playerID]->strategy(decision);
        cout <<decision<<endl;
        if (decision != "action" && decision != "done" && decision != "")
          { input = "trade"; }
        else { input = "next"; }
      }
      
    } else {
      isCOM = false;
      cout << "Please type command: ";
      cin >> input;
      if (cin.eof()) {
        cout << "Invalid input. If you want to quit game, type 'quit'" << endl;
        cin.clear();
        cin.ignore();
        continue;
      }
    }

    // if input is quit (wants to end the game)
    if (input == "quit") {
      cout << "-------CONFIRMATION-------" << endl;
      cout << "You wish to end the game =(" << endl;
      cout << "Please confirm with 'yes' ";
      cout << "(other input will be recognized as 'no' XD): ";
      cin >> input;
      if (!cin) {
        cin.clear();
        cin.ignore();
        cout << "Invalid input. Operation aborted" << endl;
        continue;
      }
      
      if (input == "yes") { return 0; }
      else { continue; }
      
    } // end if quit

    // if input is roll
    if (input == "roll") {
      if (rollNum == 0 || dcStatus != -1) {
        if (isCOM) { break; }
        cout << "-------WARNING-------" << endl;
        cout << "You cannot roll in this turn" << endl;
        cout << "---------------------" << endl;
        continue;
      }
      
      rollNum--;

      int dice1, dice2;
      if (isTesting && !isCOM) {
        cin >> dice1 >> dice2;
        if(!cin) {
          cin.clear();
          continue;
        }
      } else {
        dice1 = b.giveDice();
        dice2 = b.giveDice();
      }

      cout << "-------INFORMATION-------" << endl;
      b.playerList[playerID]->changePosition(dice1+dice2);
      b.draw();
      
      // check if passes through CollectOSAP
      if (position > b.playerList[playerID]->getPosition() && b.playerList[playerID]->getPosition() != 0) {
        b.collectOSAP(playerID);
      }

      while (1) {
        position = b.playerList[playerID]->getPosition();
        // if ownable property
        if (b.isOwnable[position] && !b.buildingList[position]->getMortgage() &&
            b.buildingList[position]->getOwner() != piece) {
          // if owner is other player; current player must pay tuition
          string ownerPiece = b.buildingList[position]->getOwner();
          if (ownerPiece != "BANK") {
            int price;
            if (b.isAcademic[position]) {
              price = b.buildingList[position]->getFees();
            } else {
              price = b.buildingList[position]->getFees(ownerPiece);
            }
            b.playerList[playerID]->pay(price,b.IDList[ownerPiece]);
            if (!b.playerList[playerID]->getBankruptcy()) {
              b.playerList[playerID]->strategy(decision);
              decision = "";
            } else {
              input = "next";
              break;
            }
            break;
          }
          
          // if owner is BANK; current player can buy the property
          else if (ownerPiece == "BANK") {
            b.playerList[playerID]->strategy(decision);
              decision = "";
            if (!isCOM) { b.playerList[playerID]->buy(position); }
            b.draw();
            break;
          }
        // if non-ownable property
        } else if (position == 2 || position == 17 || position == 33) {
          b.atSLC(playerID);
          continue;
        } else if (position == 4) {
          b.playerList[playerID]->strategy(decision);
          b.payTuition(playerID,decision);
          decision = "";
          break;
        } else if (position == 7 || position == 22 || position == 36) {
          b.atNeedlesHall(playerID);
          break;
        } else if (position == 20) {
          b.gooseNesting(playerID);
          break;
        } else if (position == 30) {
          b.playerList[playerID]->strategy(decision);
          b.goToTims(playerID,decision);
          dcChecked = true;
          decision = "";
          if (b.playerList[playerID]->getDCStatus() == -1) { rollNum = 0; }
          break;
        } else if (position == 38) {
          b.payCoopFee(playerID);
          break;
        } else if (position == 0) {
          b.collectOSAP(playerID);
          break;
        } else { // at DC Tims Line
          break;
        }
      } // end while-loop

      if (b.playerList[playerID]->getBankruptcy()) { continue; }
      
      // check if rolled doubles
      if (dice1 == dice2) {
        rollNum++;
        rollCount++;
        cout << "-------INFORMATION-------" << endl;
        cout << piece << " (" << name << ") rolled doubles. ";
        cout << piece << " (" << name << ") can roll again" << endl;
      }

      // check if rolled doubles three times
      if (rollCount == 3) {
        cout << "You seem like staying up all night programming";
        cout << " and need caffeine" << endl;
        b.playerList[playerID]->setPosition(30);
        b.playerList[playerID]->strategy(decision);
        b.goToTims(playerID,decision);
        dcChecked = true;
        decision = "";
      }
    } // end if roll

    // if input is next
    else if (input == "next") {
      if (rollNum != 0 && dcStatus == -1) {
        cout << "You need 'roll' before giving control to the next player" << endl;
        continue;
      }
      playerID++;
      if (playerID > numPlayer) { playerID-=numPlayer; }
      rollNum = 1;
      rollCount = 0;
      decision = "";
      dcChecked = false;
      cout << piece << " (" << name << ") finished the turn" << endl;
      cout << "-----------------------------" << endl << endl;
      b.draw();
    } // end if next

    // if input is trade
    else if (input == "trade") {
        bool isValidName = false;
        bool isValidProperty1 = false;
        bool isValidProperty2 = false;
        string oppoPiece;
        string give;
        string receive;
        int money;
        int money2;
        string propertyName;
        string propertyName2;
        int buildID1;  // buildingID for give
        int buildID2;  // buildingID for receive
        bool moneyPro = false;  // give is money and receive is property
        bool proMoney = false;  // give is property and receive is money
        bool proPro = false;    // give is property and receive is property
        if (decision != "") {
            stringstream ssTrade(decision);
            ssTrade >> oppoPiece >> give >> receive;
        } else {
            cin >> oppoPiece >> give >> receive;
            if (!cin) {
                cin.clear();
                cin.ignore();
                cout << "Invalid input. Please try again" << endl << endl;
                continue;
            }
        }
        
        // check whether the oppoPiece is valid
        for(int i=1;i<7;i++){
            if( b.playerList[i]!=NULL && b.playerList[i]->getPiece() == oppoPiece){
                isValidName = true;
                break;
            }
        }
        
        
        if (!isValidName) {
            cout << "-------WARNING-------" << endl;
            cout << oppoPiece << " is not a valid name or there is no player with this name" << endl;
            cout << "Please try again" << endl << endl;
            continue;
        }
        
        
        string myName = b.playerList[playerID]->getPiece() + " (" + b.playerList[playerID]->getName() + ")";
        string playerPiece = b.playerList[playerID]->getPiece();
        int oppoID = b.IDList[oppoPiece];
        string oppoName = b.playerList[oppoID]->getPiece() + " (" + b.playerList[oppoID]->getName() + ")";
        istringstream buffer1(give);
        istringstream buffer2(receive);
        if(buffer1 >> money){
            if(b.playerList[playerID]->getSavings() < money) {
                cout << "-------WARNING-------" << endl;
                cout << myName << "cannot pay so much money" << endl;
                cout << "Please try again" << endl;
                continue;
            }
            if(buffer2 >> money2) {
                cout << "-------WARNING-------" << endl;
                cout << "Trading between money and money is not allowed" << endl;
                cout << "Please try again" << endl;
                continue;
            }
            moneyPro = true;
            
            // check if valid input for the receive property
            for (int i=0; i<40; i++) {
                if (b.isOwnable[i] && receive == b.buildingList[i]->getName() && b.buildingList[i]->getOwner() == oppoPiece) {
                    isValidProperty2 = true;
                    buildID2 = i;
                    break;
                }
            }

            if (!isValidProperty2) {
                cout << "-------WARNING-------" << endl;
                cout << receive << " is not an ownable building or " << oppoName << " does not own " << receive << endl;
                cout << "Please try again" << endl << endl;
                continue;
            }
            propertyName2 = receive;
            cout << "-------INFORMATION--------" << endl;
            cout << myName << " want to use $" << money << " to buy ";
            cout << propertyName2 << " owned by " << oppoName << endl;
        }else{
            // check if valid input for the give property
            for (int i=0; i<40; i++) {
                if (b.isOwnable[i] && give == b.buildingList[i]->getName() && b.buildingList[i]->getOwner() == playerPiece) {
                    isValidProperty1 = true;
                    buildID1 = i;
                    break;
                }
            }
            
            
            if (!isValidProperty1) {
                cout << "-------WARNING-------" << endl;
                cout << give << " is not an ownable building or " << myName << " does not own " << give << endl;
                cout << "Please try again" << endl << endl;
                continue;
            }

            propertyName = give;
            
            if(buffer2 >> money){
                proMoney = true;
                cout << "-------INFORMATION--------" << endl;
                cout << myName << " want to sell " << propertyName << " to " << oppoName;
                cout << " and the price is $" << money << endl;
            }
            else {
                proPro = true;
                // check if valid input for the receive property
                for (int i=0; i<40; i++) {
                    if (b.isOwnable[i] && receive == b.buildingList[i]->getName() && b.buildingList[i]->getOwner() == oppoPiece) {
                        isValidProperty2 = true;
                        buildID2 = i;
                        break;
                }
            }
                
                
                if (!isValidProperty2) {
                   cout << "-------WARNING-------" << endl;
                   cout << receive << " is not an ownable building or " << oppoName << " does not own " << receive << endl;
                   cout << "Please try again" << endl << endl;
                   continue;
                }
              propertyName2 = receive;
              cout << "-------INFORMATION--------" << endl;
              cout << myName << "want to use " << propertyName << " to exchange ";
              cout << propertyName2 << " owned by " << oppoName << endl;
            }
    }
    string decision;
    ostringstream oss;
    oss << oppoID << " " << receive << " " << give;
    decision = oss.str();
    string choice = decision;
    b.playerList[oppoID]->strategy(decision);
    if(decision == choice) {
        cout << "------"<< oppoName << "'s selection--------" << endl;
        while (1) {
            cout << "Do you agree with this trading? ('yes' or 'no')";
            cin >> input;
            if (!cin || (input != "yes" && input != "no")) {
            cout << "Invalid input" << endl;
            if (!cin) { cin.clear();}
            }else if (input == "yes" && proMoney && b.playerList[oppoID]->getSavings() < money){
                cout << "-------WARNING-------" << endl;
                cout << oppoName << " does not have enough money to accept this trading" << endl;
                cout << "Please consider again" << endl;
            } else { break; }
        }
    } else { input = decision; }
    decision = "";
    cout << "--------INFORMATION---------" << endl;
    if (input == "yes") {
        cout << oppoName << " accepted the trading" << endl;
        if(moneyPro) {
            b.playerList[playerID]->pay(money,oppoID);
            b.buildingList[buildID2]->setOwner(playerPiece);
            moneyPro = false;
            b.playerList[playerID]->rejected[buildID2] = "";
        }
        if(proMoney) {
            b.buildingList[buildID1]->setOwner(oppoPiece);
            b.playerList[oppoID]->pay(money,playerID);
            proMoney = false;
            b.playerList[playerID]->rejected[buildID1] = "";
        }
        if(proPro) {
            b.buildingList[buildID1]->setOwner(oppoPiece);
            b.buildingList[buildID2]->setOwner(playerPiece);
            proPro = false;
            b.playerList[playerID]->rejected[buildID1] = "";
            b.playerList[playerID]->rejected[buildID2] = "";
        }
        b.draw();
    }
    else {
      cout << oppoPiece << " rejected the trading" << endl;
      if(b.playerList[playerID]->getName() == "NORMAL COM") {
                if(moneyPro) {
                  b.playerList[playerID]->rejected[buildID2] = oppoPiece;
                }
                if(proMoney){
                  b.playerList[playerID]->rejected[buildID1] = oppoPiece;
                  cout << b.playerList[playerID]->rejected[buildID1] << endl;
                }
                if(proPro){
                    b.playerList[playerID]->rejected[buildID1] = oppoPiece;
                    b.playerList[playerID]->rejected[buildID2] = oppoPiece;
                }
            }
        }
        cout << "----------------------" << endl << endl;
    }// end if trade

    // if input is impove
    else if (input == "improve") {
      bool isValid = false;
      string action;
      cin >> buildingName >> action;
      if (!cin) {
        cin.clear();
        cin.ignore();
        cout << "Invalid input. Please try again" << endl << endl;
        continue;
      }

      // check if valid input for the property
      for (int i=0; i<40; i++) {
        if (b.isOwnable[i] && buildingName == b.buildingList[i]->getName()) {
          if (b.isAcademic[i]) {
            isValid = true;
            buildingID = i;
          }
          break;
        }
      }
      if (!isValid) {
        cout << buildingName << " is not an Academic building" << endl;
        cout << "Please try again" << endl << endl;
        continue;
      }

      int i = b.buildingList[buildingID]->getImprovement();
      // check if valid input for the action
      if (action == "buy") {
        b.playerList[playerID]->changeImprovement(buildingID,'+');
        if (i != b.buildingList[buildingID]->getImprovement()) {
          b.draw();
        }
      } else if (action == "sell") {
        b.playerList[playerID]->changeImprovement(buildingID,'-');
        if (i != b.buildingList[buildingID]->getImprovement()) {
          b.draw();
        }   
      } else {
        cout << "Invalid input. Please try again" << endl << endl;
        continue;
      }
    } // end if improve

    // if input is mortgage or unmortgage
    else if (input == "mortgage" || input == "unmortgage") {
      bool isValid = false;
      cin >> buildingName;
      if (!cin) {
        cin.clear();
        cout << "Invalid input. Please try again" << endl << endl;
        continue;
      }

      // check if valid input for the property
      for (int i=0; i<40; i++) {
        if (b.isOwnable[i] && buildingName == b.buildingList[i]->getName()) {
          if (b.isOwnable[i]) {
            isValid = true;
            buildingID = i;
          }
          break;
        }
      }
      if (!isValid) {
        cout << buildingName << " is not an Ownable building" << endl;
        cout << "Please try again" << endl << endl;
        continue;
      }

      bool isMortgaged = b.buildingList[buildingID]->getMortgage();
      if (input == "mortgage") {
        b.playerList[playerID]->changeMortgage(buildingID,'+');
      } else if (input == "unmortgage") {
        b.playerList[playerID]->changeMortgage(buildingID,'-');
      }
      if(isMortgaged != b.buildingList[buildingID]->getMortgage()) {
        b.draw();
      }
         
    } // end if mortgage or unmortgage

    // if input is assets
    else if (input == "assets") {
      b.playerList[playerID]->giveAsset();
    } // end if assets

    // if input is save
    else if (input == "save") {
      string filename;
      cin >> filename;
      if (!cin) {
        cin.clear();
        cout << "-------WARNING-------" << endl;
        cout << "Invalid input. Please try again" << endl << endl;
        continue;
      }

      fstream f(filename.c_str(),fstream::out);
      f << numPlayer << " " << actualNumPlayer << " ";
      if (b.give500) f << "true" << endl;
      else  f << "false" << endl;
      for (int i=0; i<numPlayer; i++) {
        if (!b.playerList[playerID]) {continue;}
        name = b.playerList[playerID]->getName();
        piece = b.playerList[playerID]->getPiece();
        savings = b.playerList[playerID]->getSavings();
        position = b.playerList[playerID]->getPosition();
        int rurc = b.playerList[playerID]->getRurc();
        f << name << " " << piece << " " << savings << " " << " " << rurc;
        f << " " << position;
        if (position == 10) {
          dcStatus = b.playerList[playerID]->getDCStatus();
          if (dcStatus == -1) {
            f << " 0";
          } else {
            f << " " << dcStatus << " ";
            if (dcChecked == true) { f << "true"; }
            else { f << "false"; }
          }
        }
        if (i == 0) {
          f << " " << rollNum << " " << rollCount;
        }
        f << endl;

        playerID++;
        if (playerID > numPlayer) { playerID-=numPlayer; }
      }

      // output buildings
      for (int i=0; i<40; i++) {
        if (b.isOwnable[i]) {
          string buildingName = b.buildingList[i]->getName();
          string ownerPiece = b.buildingList[i]->getOwner();
          f << buildingName << " " << ownerPiece;
          if (b.isAcademic[i]) {
            int improvement = b.buildingList[i]->getImprovement();
            f << " " << improvement;
          }
          if (b.buildingList[i]->getMortgage()) { f << " " << "true"; }
          else { f << " " << "false"; }
          f << endl;
        }
      }
      cout << "-------INFORMATION-------" << endl;
      cout << "You saved the game state in file '" << filename << "'" << endl;
    } // end if save

    // if input is help
    else if (input == "help") {
      printHelp();
    } // end if help

    // if input is info
    else if (input == "info") {
      bool isValid = false;
      bool isAcademic = false;
      cin >> buildingName;
      if(!cin) {
        cin.clear();
        cin.ignore();
        cout << "Invalid input. Please try again" << endl;
        continue;
      }

      // check if valid input for the property
      for (int i=0; i<40; i++) {
        if (b.isOwnable[i]) {
          if (buildingName == b.buildingList[i]->getName()) {
            isValid = true;
            buildingID = i;
            if (b.isAcademic[i]) {
              isAcademic = true;
            }
          }
        }
      }
      
      if (!isValid) {
        cout << buildingName << " is not an Ownable building" << endl;
        cout << "Please try again" << endl;
        continue;
      }
      
      string ownerPiece = b.buildingList[buildingID]->getOwner();
      string ownerName = b.nameList[ownerPiece];
      cout << "-------" << buildingName << "-------" << endl;
      cout << "Owner: " << ownerPiece << " (" << ownerName << ")" << endl;
      cout << "Cost: " << b.buildingList[buildingID]->getPurchaseCost() << endl;
      if (isAcademic) {
        int improvement = b.buildingList[buildingID]->getImprovement();
        int improCost = b.buildingList[buildingID]->getImprovementCost();
        cout << "Upgraded: " << improvement << " time(s)     ";
        cout << "Next Improvement Cost: " << improCost << endl;
      }
    } // end if info

    // if invalid input
    else {
      cout << "You typed a invalid input" << endl;
      cout << "Available commands can be checked by typing 'help'" << endl;
    }
  }
  
}
