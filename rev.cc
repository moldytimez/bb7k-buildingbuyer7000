//
//  rev.cpp
//  bb7k
//
//  Created by jessica on 14/11/20.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "rev.h"
#include <iostream>
#include <string>
#include "residence.h"
#include "building.h"
using namespace std;

REV::REV():Residence("REV"){}


string REV::getOwner(){
    return owner;
}

void REV::setOwner(string ownerPiece){
    resCount[owner]--;
    owner = ownerPiece;
    resCount[owner]++;
}

int REV::getFees(){
    return rent;
}

int REV::getFees(string ownerPiece){
    cout << "-------INFORMATION-------" << endl;
    cout << "Fees to pay depend on number of residences the owner owns" << endl;
    cout << "The owner owns " << resCount[ownerPiece] << " residence(s)" << endl;
    rent = resCount[ownerPiece]*25;
    return rent;
}


int REV::getPurchaseCost(){
    return purchaseCost;
}

string REV::getName(){
    return name;
}



bool REV::getMortgage(){
    return isMortgage;
}

void REV::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}


int REV::getImprovementCost(){return 0;};
void REV::changeImprovement(string act){}
int REV::getImprovement(){return 0;}
bool REV::checkMonopoly(bool whetherPrint){ return false;}
bool REV::checkMonopoly(){return false;}

REV::~REV(){}

