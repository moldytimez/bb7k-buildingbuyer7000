//
//  v1.cpp
//  bb7k
//
//  Created by jessica on 14/11/20.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "v1.h"
#include <iostream>
#include <string>
#include "residence.h"
#include "building.h"
using namespace std;

V1::V1():Residence("V1"){}

string V1::getOwner(){
    return owner;
}

void V1::setOwner(string ownerPiece){
    resCount[owner]--;
    owner = ownerPiece;
    resCount[owner]++;
}

int V1::getFees(){
    return rent;
}

int V1::getFees(string ownerPiece){
    cout << "-------INFORMATION-------" << endl;
    cout << "Fees to pay depend on number of residences the owner owns" << endl;
    cout << "The owner owns " << resCount[ownerPiece] << " residence(s)" << endl;
    rent = resCount[ownerPiece]*25;
    return rent;
}


int V1::getPurchaseCost(){
    return purchaseCost;
}

string V1::getName(){
    return name;
}



bool V1::getMortgage(){
    return isMortgage;
}

void V1::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

int V1::getImprovementCost(){return 0;};
void V1::changeImprovement(string act){}
int V1::getImprovement(){return 0;}
bool V1::checkMonopoly(bool whetherPrint){ return false;}
bool V1::checkMonopoly(){return false;}
V1::~V1(){}


