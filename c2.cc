//
//  c2.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "c2.h"
#include "sci2.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

C2::C2():SCI2(200,320,"C2"){
    tuitionList[0] = 28;
    tuitionList[1] = 150;
    tuitionList[2] = 450;
    tuitionList[3] = 1000;
    tuitionList[4] = 1200;
    tuitionList[5] = 1400;
}


int C2::getPurchaseCost(){
    return purchaseCost;
}

int C2::getImprovementCost(){
    return improvementCost;
}

void C2::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["C2"] = improvement;
}


int C2::getImprovement(){
    return improvement;
}

bool C2::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["EIT"] == owner && ownerList["ESC"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["EIT"] == owner && ownerList["ESC"] != owner)
            cout << "The property required to be owned is ESC (owner: " << ownerList["ESC"] << ")" << endl;
        if(ownerList["EIT"] != owner && ownerList["C2"] == owner)
            cout << "The property required to be owned is EIT (owner: " << ownerList["EIT"] << ")" << endl;
        if(ownerList["EIT"] != owner && ownerList["ESC"] != owner){
            cout << "The property required to be owned is EIT (owner: " << ownerList["EIT"] << ")" << endl;
            cout << "The property required to be owned is ESC (owner: " << ownerList["ESC"] << ")" << endl;
        }
    }
    return isMonopoly;
}


bool C2::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["EIT"] == owner && ownerList["ESC"]== owner) isMonopoly = true;
    return isMonopoly;
}


int C2::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["EIT"] == 0 && improvementList["ESC"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int C2::getFees(string ownerPiece){return 0;}

string C2::getName(){
    return name;
}

string C2::getOwner(){
    return owner;
}

void C2::setOwner(string name){
    owner = name;
    ownerList["C2"] = name;
}


bool C2::getMortgage(){
    return isMortgage;
}

void C2::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

C2::~C2(){}


