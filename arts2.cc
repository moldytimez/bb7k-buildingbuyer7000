//
//  arts2.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "arts2.h"
#include <map>
#include <string>
#include "academic.h"
using namespace std;


map<string,string> Arts2::ownerList;
map<string,int> Arts2::improvementList;


Arts2::Arts2(int improveCost,int purchaseCost,std::string name):Academic(improveCost,purchaseCost,name){
    ownerList["HH"] = "BANK";
    ownerList["PAS"] = "BANK";
    ownerList["ECH"] = "BANK";
    improvementList["HH"] = 0;
    improvementList["PAS"] = 0;
    improvementList["ECH"] = 0;
}


Arts2::~Arts2(){}