//
//  health.h
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#ifndef __HEALTH_H__
#define __HEALTH_H__

#include <map>
#include <string>
#include "academic.h"

class Health:public Academic{
protected:
    static std::map<std::string,std::string> ownerList;
    static std::map<std::string,int> improvementList;
public:
    Health(int improveCost,int purchaseCost,std::string name);
    ~Health();
};

#endif 
