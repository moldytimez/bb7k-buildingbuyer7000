//
//  arts2.h
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#ifndef __ARTS2_H__
#define __ARTS2_H__

#include <map>
#include <string>
#include "academic.h"

class Arts2:public Academic{
protected:
    static std::map<std::string,std::string> ownerList;
    static std::map<std::string,int> improvementList;
public:
    Arts2(int improveCost,int purchaseCost,std::string name);
    ~Arts2();
};

#endif
