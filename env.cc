//
//  env.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "env.h"
#include <map>
#include <string>
#include "academic.h"
using namespace std;


map<string,string> ENV::ownerList;
map<string,int> ENV::improvementList;

ENV::ENV(int improveCost,int purchaseCost,std::string name):Academic(improveCost,purchaseCost,name){
    ownerList["EV1"] = "BANK";
    ownerList["EV2"] = "BANK";
    ownerList["EV3"] = "BANK";
    improvementList["EV1"] = 0;
    improvementList["EV2"] = 0;
    improvementList["EV3"] = 0;
}

ENV::~ENV(){}