//
//  uwp.h
//  bb7k
//
//  Created by jessica on 14/11/20.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#ifndef __UWP_H__
#define __UWP_H__

#include <iostream>
#include <string>
#include "residence.h"
#include "building.h"

class UWP: public Residence{
public:
    UWP();
    std::string getOwner();
    void setOwner(std::string);
    int getFees();
    int getFees(std::string);
    int getPurchaseCost();
    std::string getName();
    bool getMortgage();
    void setMortgage(bool whetherMortgage);
    int getImprovementCost();
    void changeImprovement(std::string act);
    int getImprovement();
    bool checkMonopoly(bool whetherPrint);
    bool checkMonopoly();
    ~UWP();
};

#endif
