//
//  esc.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "esc.h"
#include "sci2.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

ESC::ESC():SCI2(200,300,"ESC"){
    tuitionList[0] = 26;
    tuitionList[1] = 130;
    tuitionList[2] = 390;
    tuitionList[3] = 900;
    tuitionList[4] = 1100;
    tuitionList[5] = 1275;
}


int ESC::getPurchaseCost(){
    return purchaseCost;
}

int ESC::getImprovementCost(){
    return improvementCost;
}

void ESC::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["ESC"] = improvement;
}


int ESC::getImprovement(){
    return improvement;
}

bool ESC::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["EIT"] == owner && ownerList["C2"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["EIT"] == owner && ownerList["C2"] != owner)
            cout << "The property required to be owned is C2 (owner: " << ownerList["C2"] << ")" << endl;
        if(ownerList["EIT"] != owner && ownerList["C2"] == owner)
            cout << "The property required to be owned is EIT (owner: " << ownerList["EIT"] << ")" << endl;
        if(ownerList["EIT"] != owner && ownerList["C2"] != owner){
            cout << "The property required to be owned is EIT (owner: " << ownerList["EIT"] << ")" << endl;
            cout << "The property required to be owned is C2 (owner: " << ownerList["C2"] << ")" << endl;
        }
    }
    return isMonopoly;
}


bool ESC::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["EIT"] == owner && ownerList["C2"]== owner) isMonopoly = true;
    return isMonopoly;
}


int ESC::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["EIT"] == 0 && improvementList["C2"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int ESC::getFees(string ownerPiece){return 0;}

string ESC::getName(){
    return name;
}

string ESC::getOwner(){
    return owner;
}

void ESC::setOwner(string name){
    owner = name;
    ownerList["ESC"] = name;
}


bool ESC::getMortgage(){
    return isMortgage;
}

void ESC::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

ESC::~ESC(){}


