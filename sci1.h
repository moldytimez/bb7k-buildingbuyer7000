//
//  sci1.h
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#ifndef __SCI1_H__
#define __SCI1_H__

#include <map>
#include <string>
#include "academic.h"

class SCI1:public Academic{
protected:
    static std::map<std::string,std::string> ownerList;
    static std::map<std::string,int> improvementList;
public:
    SCI1(int improveCost,int purchaseCost,std::string name);
    ~SCI1();
};

#endif 
