//
//  phys.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "phys.h"
#include "sci1.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

PHYS::PHYS():SCI1(150,260,"PHYS"){
    tuitionList[0] = 22;
    tuitionList[1] = 110;
    tuitionList[2] = 330;
    tuitionList[3] = 800;
    tuitionList[4] = 975;
    tuitionList[5] = 1150;
}


int PHYS::getPurchaseCost(){
    return purchaseCost;
}

int PHYS::getImprovementCost(){
    return improvementCost;
}

void PHYS::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["PHYS"] = improvement;
}


int PHYS::getImprovement(){
    return improvement;
}

bool PHYS::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["B1"] == owner && ownerList["B2"]== owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        if(ownerList["B1"] == owner && ownerList["B2"] != owner)
            cout << "The property required to be owned is B2 (owner: " << ownerList["B2"] << ")" << endl;
        if(ownerList["B1"] != owner && ownerList["B2"] == owner)
            cout << "The property required to be owned is B1 (owner: " << ownerList["B1"] << ")" << endl;
        if(ownerList["B1"] != owner && ownerList["B2"] != owner){
            cout << "The property required to be owned is B1 (owner: " << ownerList["B1"] << ")" << endl;
            cout << "The property required to be owned is B2 (owner: " << ownerList["B2"] << ")" << endl;
        }
    }
    return isMonopoly;
}


bool PHYS::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["B1"] == owner && ownerList["B2"]== owner) isMonopoly = true;
    return isMonopoly;
}


int PHYS::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["B1"] == 0 && improvementList["B2"] == 0)
        tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int PHYS::getFees(string ownerPiece){return 0;}

string PHYS::getName(){
    return name;
}

string PHYS::getOwner(){
    return owner;
}

void PHYS::setOwner(string name){
    owner = name;
    ownerList["PHYS"] = name;
}


bool PHYS::getMortgage(){
    return isMortgage;
}

void PHYS::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

PHYS::~PHYS(){}
