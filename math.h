//
//  math.h
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#ifndef __MATH_H__
#define __MATH_H__

#include <map>
#include <string>
#include "academic.h"

class Math:public Academic{
protected:
    static std::map<std::string,std::string> ownerList;
    static std::map<std::string,int> improvementList;
public:
    Math(int improveCost,int purchaseCost,std::string name);
    ~Math();
};

#endif 
