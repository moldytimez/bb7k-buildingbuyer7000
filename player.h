#ifndef __PLAYER_H__
#define __PLAYER_H__
#include <string>
#include <map>

class Board;
class Building;

class Player {
 protected:
  int playerID;                         // player ID (0..5)
  std::string name;                     // player's name
  std::string piece;                    // selected chess piece
  int savings;                          // amount of savings
  int position;                         // current at building ID
  int rurc;                             // counts number of rurc owned
  bool isBankrupt;                      // true if bankrupt; false otherwise
  int dcStatus;                         // -1 for not in DCTimesLine;
                                        // 0..2 for turns in DCTimesLine
    /* for graphic display
     int x,y; */

  Building **buildingList;              // will be initialized in Board class
 public:
  static int rurcCount;                 // rurcCount <= 4 during the entire game
  // data of the following five would be given in Board class
  static Player *playerList[7];
  // piece as index, playerID 
  static std::map<std::string,int> IDList;
  // piece as index, name as value; used for nice printing
  static std::map<std::string,std::string> nameList;
  static bool *isOwnable;            // true if ownable property,false otherwise
  static bool *isAcademic;           // true if academic block,false otherwise
  std::string rejected[40];          // store ownerPiece if trade of building is rejected
  
  Player(int,std::string,std::string,int,int,int,Building**);
  virtual ~Player();                      // dtor

  virtual int getID()=0;                  // return the playerID field
  virtual std::string getName()=0;        // return the name field
  virtual std::string getPiece()=0;       // return the piece field
  virtual int getSavings()=0;             // return the savings field
  virtual void setSavings(int)=0;         // set the savings field
  virtual int getPosition()=0;            // return the current building ID)
  virtual void setPosition(int)=0;        // set the current position (building ID)
  virtual void changePosition(int)=0;     // change the current position by int
  virtual int getDCStatus()=0;            // return the dcStatus field
  virtual void setDCStatus(int)=0;        // set the dcStatus field
  virtual int getRurc()=0;                // return the rurc field
  /* for graphic display
     virtual int getX();
     virtual int getY();
     virtual void setCoords(int,int);*/
  virtual void changeRurc(int)=0;         // +/- rurc by int
  virtual void changeSavings(int)=0;      // +/- savings by int
  virtual void pay(int,int)=0;            // pay given amount to given ownerID
  virtual void buy(int)=0;                // buy building given its ID
  virtual void changeImprovement(int,char)=0; // '+'/'-' improvement given ID
  virtual void changeMortgage(int,char)=0;// (un)mortgage the building given its ID
  virtual void giveAsset()=0;             // print out the asset information
  virtual void bankruptProcess(int,int)=0; // whether bankrupt or sell stuff
  virtual bool getBankruptcy()=0;          // return the isBankrupt field
  virtual void setBankruptcy(bool)=0;      // set the isBankrupt field
  virtual void strategy(std::string&)=0;   // computer abstract behaviour
    
};
#endif
