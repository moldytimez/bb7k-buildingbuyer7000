//
//  board.cpp
//  bb7k
//
//  Created by jessica on 14/11/24.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "board.h"
#include "bank.h"
#include "player.h"
#include "human.h"
#include "easycomputer.h"
#include "normalcomputer.h"
#include "building.h"
#include "al.h"
#include "ml.h"
#include "ech.h"
#include "pas.h"
#include "hh.h"
#include "rch.h"
#include "dwe.h"
#include "cph.h"
#include "lhi.h"
#include "bmh.h"
#include "opt.h"
#include "ev1.h"
#include "ev2.h"
#include "ev3.h"
#include "phys.h"
#include "b1.h"
#include "b2.h"
#include "eit.h"
#include "esc.h"
#include "c2.h"
#include "mc.h"
#include "dc.h"
#include "mkv.h"
#include "uwp.h"
#include "v1.h"
#include "rev.h"
#include "pac.h"
#include "cif.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
using namespace std;




Board::Board(){
    give500 = true;
    for(int i=0;i<40;i++){
        isAcademic[i] = false;
        isOwnable[i] = true;
    }
    buildingList[0] = NULL;
    isOwnable[0] = false;
    buildingList[1] = new AL;
    isAcademic[1] = true;
    buildingList[2] = NULL;
    isOwnable[2] = false;
    buildingList[3] = new ML;
    isAcademic[3] = true;
    buildingList[4] = NULL;
    isOwnable[4] = false;
    buildingList[5] = new MKV;
    buildingList[6] = new ECH;
    isAcademic[6] = true;
    buildingList[7] = NULL;
    isOwnable[7] = false;
    buildingList[8] = new PAS;
    isAcademic[8] = true;
    buildingList[9] = new HH;
    isAcademic[9] = true;
    buildingList[10] = NULL;
    isOwnable[10] = false;
    buildingList[11] = new RCH;
    isAcademic[11] = true;
    buildingList[12] = new PAC;
    buildingList[13] = new DWE;
    isAcademic[13] = true;
    buildingList[14] = new CPH;
    isAcademic[14] = true;
    buildingList[15] = new UWP;
    buildingList[16] = new LHI;
    isAcademic[16] = true;
    buildingList[17] = NULL;
    isOwnable[17] = false;
    buildingList[18] = new BMH;
    isAcademic[18] = true;
    buildingList[19] = new OPT;
    isAcademic[19] = true;
    buildingList[20] = NULL;
    isOwnable[20] = false;
    buildingList[21] = new EV1;
    isAcademic[21] = true;
    buildingList[22] = NULL;
    isOwnable[22] = false;
    buildingList[23] = new EV2;
    isAcademic[23] = true;
    buildingList[24] = new EV3;
    isAcademic[24] = true;
    buildingList[25] = new V1;
    buildingList[26] = new PHYS;
    isAcademic[26] = true;
    buildingList[27] = new B1;
    isAcademic[27] = true;
    buildingList[28] = new CIF;
    buildingList[29] = new B2;
    isAcademic[29] = true;
    buildingList[30] = NULL;
    isOwnable[30] = false;
    buildingList[31] = new EIT;
    isAcademic[31] = true;
    buildingList[32] = new ESC;
    isAcademic[32] = true;
    buildingList[33] = NULL;
    isOwnable[33] = false;
    buildingList[34] = new C2;
    isAcademic[34] = true;
    buildingList[35] = new REV;
    buildingList[36] = NULL;
    isOwnable[36] = false;
    buildingList[37] = new MC;
    isAcademic[37] = true;
    buildingList[38] = NULL;
    isOwnable[38] = false;
    buildingList[39] = new DC;
    isAcademic[39] = true;
    playerList[0] = new Bank(buildingList);
    for(int i=1;i<7;i++){
      playerList[i] = NULL;
    }
    IDList["BANK"] = 0;
    nameList["BANK"] = "BANK";
}

Board::~Board(){
    for(int i=0;i<40;i++)
        delete buildingList[i];
}


void Board::addPlayer(int playerID, string name, string piece, int savings, int position,int rurc){
    IDList[piece] = playerID;
    nameList[piece] = name;
    if (name == "EASY COM") {
      playerList[playerID] = new EasyComputer(playerID,name,piece,savings,position,rurc,buildingList);
    } else if (name == "NORMAL COM") {
      playerList[playerID] = new NormalComputer(playerID,name,piece,savings,position,rurc,buildingList);
    } else {
      playerList[playerID] = new Human(playerID,name,piece,savings,position,rurc,buildingList);
    }
    playerList[playerID]->IDList = IDList;
    playerList[playerID]->nameList = nameList;
    playerList[playerID]->isAcademic = isAcademic;
    playerList[playerID]->isOwnable = isOwnable;
}

void Board::addPlayerList() {
    for(int i=0;i<7;i++) {
        if(playerList[i]) {
            for(int j=0;j<7;j++) {
                playerList[i]->playerList[j] = playerList[j];
            }
        }
    }
}

// print the improvement of the building

void Board::printI(int buildingID){
    int numI = buildingList[buildingID]->getImprovement();
    for(int i=1;i<=8;i++){
        if(i<=numI){
            cout << "I";
        }else{
            cout <<" ";
        }
    }
}

// print the player piece in the first row

void Board::printName1(int buildingID){
    int recordG = 0;
    int recordB = 0;
    int recordD = 0;
    int recordP = 0;
    for(int i=1;i<7;i++){
        if(playerList[i]!= NULL && playerList[i]->getPiece() == "G") recordG = i;
        if(playerList[i]!= NULL && playerList[i]->getPiece() == "B") recordB = i;
        if(playerList[i]!= NULL && playerList[i]->getPiece() == "D") recordD = i;
        if(playerList[i]!= NULL && playerList[i]->getPiece() == "P") recordP = i;
    }
    if(recordG!=0 && playerList[recordG]->getPosition() == buildingID) cout << "G ";
    else cout <<"  ";
    if(recordB!=0 && playerList[recordB]->getPosition() == buildingID) cout << "B ";
    else cout <<"  ";
    if(recordD!=0 && playerList[recordD]->getPosition() == buildingID) cout << "D ";
    else cout <<"  ";
    if(recordP!=0 && playerList[recordP]->getPosition() == buildingID) cout << "P ";
    else cout <<"  ";
}

// print the player piece in the second row

void Board::printName2(int buildingID){
    int recordS = 0;
    int record$ = 0;
    int recordL = 0;
    int recordT = 0;
    for(int i=1;i<7;i++){
        if(playerList[i]!= NULL && playerList[i]->getPiece() == "S") recordS = i;
        if(playerList[i]!= NULL && playerList[i]->getPiece() == "$") record$ = i;
        if(playerList[i]!= NULL && playerList[i]->getPiece() == "L") recordL = i;
        if(playerList[i]!= NULL && playerList[i]->getPiece() == "T") recordT = i;
    }
    if(recordS!=0 && playerList[recordS]->getPosition() == buildingID) cout << "S ";
    else cout <<"  ";
    if(record$!=0 && playerList[record$]->getPosition() == buildingID) cout << "$ ";
    else cout <<"  ";
    if(recordL!=0 && playerList[recordL]->getPosition() == buildingID) cout << "L ";
    else cout <<"  ";
    if(recordT!=0 && playerList[recordT]->getPosition() == buildingID) cout << "T ";
    else cout <<"  ";
}


// print the owner of the building
void Board::printOwner(int buildingID) {
    if(buildingList[buildingID]->getOwner()!="BANK") {
        if(buildingList[buildingID]->getMortgage()) {
            cout << "(-" << buildingList[buildingID]->getOwner() << ")";
        }else{
            cout << " (" << buildingList[buildingID]->getOwner() << ")";
        }
    } else{
        cout << "    ";
    }
}

void breakTime() {
    clock_t temp;
    temp = clock () + 0.05 * CLOCKS_PER_SEC ;
    while (clock() < temp) {}
}

void breakTime2() {
    clock_t temp;
    temp = clock () + 1.5 * CLOCKS_PER_SEC ;
    while (clock() < temp) {}
}

void Board::draw(){
    breakTime2();
    cout << "----------------------------------------------------------------------------------------------------"<< endl;
    breakTime();
    cout << "|Goose   |"; printI(21);cout<<"|NEEDLES |";printI(23);cout<<"|";printI(24);cout<<"|V1  ";printOwner(25);cout<<"|";printI(26);cout<<"|";printI(27);cout <<"|CIF ";printOwner(28);cout<<"|";printI(29);cout<<"|GO TO   |"<< endl;
    breakTime();
    cout << "|Nesting |--------|HALL    |--------|--------|        |--------|--------|        |--------|TIMS    |"<< endl;
    breakTime();
    cout << "|        |EV1 ";printOwner(21);cout<<"|        |EV2 ";printOwner(23);cout<<"|EV3 ";printOwner(24);cout<<"|        |PHYS";printOwner(26);cout<<"|B1  ";printOwner(27);cout<<"|        |B2  ";printOwner(29);cout<<"|        |"<< endl;
    cout << "|";printName1(20);cout<<"|";printName1(21);cout<<"|";printName1(22);cout<<"|";printName1(23);cout<<"|";printName1(24);
    cout<<"|";printName1(25);cout<<"|";printName1(26);cout<<"|";printName1(27);cout<<"|";printName1(28);cout<<"|";printName1(29);cout<<"|";printName1(30);cout<<"|"<< endl;
    breakTime();
    cout << "|";printName2(20);cout<<"|";printName2(21);cout<<"|";printName2(22);cout<<"|";printName2(23);cout<<"|";printName2(24);cout<<"|";printName2(25);cout<<"|";printName2(26);cout<<"|";printName2(27);cout<<"|";printName2(28);cout<<"|";printName2(29);cout<<"|";printName2(30);cout<<"|"<< endl;
    breakTime();
    cout << "----------------------------------------------------------------------------------------------------"<< endl;
    breakTime();
    cout << "|";printI(19);cout<<"|                                                                                |";printI(31);cout<<"|"<< endl;
    breakTime();
    cout << "|--------|                                                                                |--------|"<< endl;
    breakTime();
    cout << "|OPT ";printOwner(19);cout<<"|                                                                                |EIT ";printOwner(31);cout<<"|"<< endl;
    breakTime();
    cout << "|";printName1(19);cout<<"|                                                                                |";printName1(31);cout<<"|"<< endl;
    breakTime();
    cout << "|";printName2(19);cout<<"|                                                                                |";printName2(31);cout<<"|"<< endl;
    breakTime();
    cout << "----------                                                                                ----------"<< endl;
    breakTime();
    cout << "|";printI(18);cout<<"|                                                                                |";printI(32);cout<<"|"<< endl;
    breakTime();
    cout << "|--------|                                                                                |--------|"<< endl;
    breakTime();
    cout << "|BMH ";printOwner(18);cout<<"|                                                                                |ESC ";printOwner(32);cout<<"|"<< endl;
    breakTime();
    cout << "|";printName1(18);cout<<"|                                                                                |";printName1(32);cout<<"|"<< endl;
    breakTime();
    cout << "|";printName2(18);cout<<"|                                                                                |";printName2(32);cout<<"|"<< endl;
    breakTime();
    cout << "----------                                                                                ----------"<< endl;
    breakTime();
    cout << "|SLC     |                                                                                |SLC     |"<< endl;
    breakTime();
    cout << "|        |                                                                                |        |"<< endl;
    breakTime();
    cout << "|        |                                                                                |        |"<< endl;
    breakTime();
    cout << "|";printName1(17);cout<<"|                                                                                |";printName1(33);cout<<"|"<< endl;
    breakTime();
    cout << "|";printName2(17);cout<<"|                                                                                |";printName2(33);cout<<"|"<< endl;
    breakTime();
    cout << "----------                                                                                ----------"<< endl;
    breakTime();
    cout << "|";printI(16);cout<<"|                                                                                |";printI(34);cout<<"|"<< endl;
    breakTime();
    cout << "|--------|                                                                                |--------|"<< endl;
    breakTime();
    cout << "|LHI ";printOwner(16);cout<<"|                                                                                |C2  ";printOwner(34);cout<<"|"<< endl;
    breakTime();
    cout << "|";printName1(16);cout<<"|                 ----------------------------------------------                 |";printName1(34);cout<<"|"<< endl;
    breakTime();
    cout << "|";printName2(16);cout<<"|                 |                                            |                 |";printName2(34);cout<<"|"<< endl;
    breakTime();
    cout << "----------                 |  ###   ###   #####  ###     ###     ###    |                 ----------"<< endl;
    breakTime();
    cout << "|UWP ";printOwner(15);cout<<"|                 |  #  #  #  #     #  #   #   #   #   #   #   |                 |REV ";printOwner(35);cout<<"|"<< endl;
    breakTime();
    cout << "|        |                 |  ####  ####    #   #   #   #   #   #   #   |                 |        |"<< endl;
    breakTime();
    cout << "|        |                 |  #   # #   #  #    #   #   #   #   #   #   |                 |        |"<< endl;
    breakTime();
    cout << "|";printName1(15);cout<<"|                 |  ####  ####  #      ###     ###     ###    |                 |";printName1(35);cout<<"|"<< endl;
    breakTime();
    cout << "|";printName2(15);cout<<"|                 |                                            |                 |";printName2(35);cout<<"|"<< endl;
    breakTime();
    cout << "----------                 ----------------------------------------------                 ----------"<< endl;
    breakTime();
    cout << "|";printI(14);cout<<"|                                                                                |NEEDLES |"<< endl;
    breakTime();
    cout << "|--------|                                                                                |HALL    |"<< endl;
    breakTime();
    cout << "|CPH ";printOwner(14);cout<<"|                                                                                |        |"<< endl;
    breakTime();
    cout << "|";printName1(14);cout<<"|                                                                                |";printName1(36);cout<<"|"<< endl;
    breakTime();
    cout << "|";printName2(14);cout<<"|                                                                                |";printName2(36);cout<<"|"<< endl;
    breakTime();
    cout << "----------                                                                                ----------"<< endl;
    breakTime();
    cout << "|";printI(13);cout<<"|                                                                                |";printI(37);cout<<"|"<< endl;
    breakTime();
    cout << "|--------|                                                                                |--------|"<< endl;
    breakTime();
    cout << "|DWE ";printOwner(13);cout<<"|                                                                                |MC  ";printOwner(37);cout<<"|"<< endl;
    breakTime();
    cout << "|";printName1(13);cout<<"|                                                                                |";printName1(37);cout<<"|"<< endl;
    breakTime();
    cout << "|";printName2(13);cout<<"|                                                                                |";printName2(37);cout<<"|"<< endl;
    breakTime();
    cout << "----------                                                                                ----------"<< endl;
    breakTime();
    cout << "|PAC ";printOwner(12);cout<<"|                                                                                |COOP    |"<< endl;
    breakTime();
    cout << "|        |                                                                                |FEE     |"<< endl;
    breakTime();
    cout << "|        |                                                                                |        |"<< endl;
    breakTime();
    cout << "|";printName1(12);cout<<"|                                                                                |";printName1(38);cout<<"|"<< endl;
    breakTime();
    cout << "|";printName2(12);cout<<"|                                                                                |";printName2(38);cout<<"|"<< endl;
    breakTime();
    cout << "----------                                                                                ----------"<< endl;
    breakTime();
    cout << "|";printI(11);cout<<"|                                                                                |";printI(39);cout<<"|"<< endl;
    breakTime();
    cout << "|--------|                                                                                |--------|"<< endl;
    breakTime();
    cout << "|RCH ";printOwner(11);cout<<"|                                                                                |DC  ";printOwner(39);cout<<"|"<< endl;
    breakTime();
    cout << "|";printName1(11);cout<<"|                                                                                |";printName1(39);cout<<"|"<< endl;
    breakTime();
    cout << "|";printName2(11);cout<<"|                                                                                |";printName2(39);cout<<"|"<< endl;
    breakTime();
    cout << "----------------------------------------------------------------------------------------------------"<< endl;
    breakTime();
    cout << "|DC Tims |";printI(9);cout<<"|";printI(8);cout<<"|NEEDLES |";printI(6);cout<<"|MKV ";printOwner(5);cout<<"|TUITION |";printI(3);cout<<"|SLC     |";printI(1);cout<<"|COLLECT |"<< endl;
    breakTime();
    cout << "|Line    |--------|--------|HALL    |--------|        |        |--------|        |--------|OSAP    |"<< endl;
    breakTime();
    cout << "|        |HH  ";printOwner(9);cout<<"|PAS ";printOwner(8);cout<<"|        |ECH ";printOwner(6);cout<<"|        |        |ML  ";printOwner(3);cout<<"|        |AL  ";printOwner(1);cout<<"|        |"<< endl;
    breakTime();
    cout << "|";printName1(10);cout<<"|";printName1(9);cout<<"|";printName1(8);cout<<"|";printName1(7);cout<<"|";printName1(6);cout<<"|";printName1(5);cout<<"|";printName1(4);cout<<"|";printName1(3);cout<<"|";printName1(2);cout<<"|";printName1(1);cout<<"|";printName1(0);cout<<"|"<< endl;
    breakTime();
    cout << "|";printName2(10);cout<<"|";printName2(9);cout<<"|";printName2(8);cout<<"|";printName2(7);cout<<"|";printName2(6);cout<<"|";printName2(5);cout<<"|";printName2(4);cout<<"|";printName2(3);cout<<"|";printName2(2);cout<<"|";printName2(1);cout<<"|";printName2(0);cout<<"|"<< endl;
    breakTime();
    cout << "----------------------------------------------------------------------------------------------------"<< endl;
    breakTime();
    
    

                   
                   
}


void Board::collectOSAP(int playerID){
    string printName = playerList[playerID]->getPiece() + " (" + playerList[playerID]->getName() + ")";
    cout << "-------INFORMATION-------" << endl;
    cout << "Congratulations. " << printName << " collected OSAP" << endl;
    if(playerList[playerID]->getPosition() == 0) {
        playerList[playerID]->changeSavings(400);
        cout << "by landing on CollectOSAP" << endl;
    }
    else {
        playerList[playerID]->changeSavings(200);
        cout << "by passing through CollectOSAP" << endl;
    }
    cout << "--------------------------" << endl << endl;
}


// function for rolling dice

int Board::giveDice(){
    static bool diceCalled = false;
    if(!diceCalled) {
        srand(time(NULL));
        diceCalled = true;
    }
    return rand()%6+1;
}


// function for selection of dcTimsLine

string selection1(int turn,int rurc){
    string input;
    while (1) {
        cout << "-------SELECTION-------" << endl;
        cout << "You must make a choice for leaving DC Tims Line" << endl;
        cout << "You can choose to roll dice but only rolling doubles can bring you out" << endl;
        cout << "Or you may pay $50"<<endl;
        cout << "Or you can use a Roll Up the Rim Cup if you own one"<<endl;
        cout << "-------INFORMATION-------" << endl;
        if(turn == 0){
            cout << "This is your first turn"<< endl;
        }
        if(turn == 1){
            cout << "This is your second turn"<< endl;
        }
        if(turn == 2){
            cout << "This is your third turn"<< endl;
        }
        cout << "Please type 'dice' or 'pay' or 'rimcup'";
        cout << "(other input will not be recognized): ";
        cin >> input;
        if (!cin) {
            cin.clear();
            cin.ignore();
            continue;
        } else if (input == "dice") {
            break;
        } else if (input == "pay") {
            break;
        } else if (input == "rimcup" && rurc !=0){
            break;
        } else if (input == "rimcup" && rurc == 0){
            cout << "-------WARNING-------" << endl;
            cout << "You do not own a rimcup" << endl;
            cout << "Please choose other options" << endl;
            continue;
        }else { continue; }
    }
    return input;
}


string selection2(int rurc){
    string input;
    while (1) {
        cout << "-------SELECTION-------" << endl;
        cout << "This is your third turn" << endl;
        cout << "You must leave DC Tims Line" << endl;
        cout << "You can only choose to pay $50" << endl;
        cout << "Or you can use a Roll Up the Rim cup if you own one"<<endl;
        cout << "Please type 'pay' or 'rimcup'";
        cout << "(other input will not be recognized): ";
        cin >> input;
        if (!cin) {
            cin.clear();
            continue;
        } else if (input == "pay") {
            break;
        } else if (input == "rimcup" && rurc!=0){
            break;
        } else if (input == "rimcup" && rurc == 0){
            cout << "-------WARNING-------" << endl;
            cout << "You do not own a rimcup" << endl;
            cout << "Please choose other options" << endl;
            continue;
        } else { continue; }
    }
    return input;
}






void Board::dcTimsLine(int playerID,string decision){
    int turn = playerList[playerID]->getDCStatus();
    if(playerList[playerID]->getPosition() != 10 || turn != -1 ){   // check whether the player is sent to the DCTimsLine or not
        int position = playerList[playerID]->getPosition();
        playerList[playerID]->setPosition(10);
        if(position!=playerList[playerID]->getPosition()) {
            draw();
        }
        string printName = playerList[playerID]->getPiece() + " (" + playerList[playerID]->getName() + ")";
        cout << printName<<" are trapped in the DC Tims Line" << endl;
        int rurc = playerList[playerID]->getRurc();
        string input;
        if(decision != "") { input = decision; }
        else{
            input = selection1(turn,rurc);
        }
        if(input == "dice") {
            int dice1 = giveDice();
            int dice2 = giveDice();
            if(dice1 == dice2) {
                playerList[playerID]->setDCStatus(-1);
                cout << "-------INFORMATION-------" << endl;
                cout << printName << " rolled " << dice1 << " and " << dice2 << endl;
                cout << printName<<" rolled doubles. " << printName << " can leave DC Tims Line in next turn" << endl;
            }
            if(dice1 != dice2){
                if(turn != 2){ // check whether this is the 3rd turn
                    turn += 1;
                    playerList[playerID]->setDCStatus(turn);
                    cout << "-------INFORMATION-------" << endl;
                    cout << printName << " rolled " << dice1 << " and " << dice2 << endl;
                    cout << printName<<" did not roll doubles. Next turn "<<printName<<" is still in DC Tims Line" << endl;
                }
                else{
                  if (decision == "dice") { input = "pay"; }
                  else {
                      cout << "-------INFORMATION-------" << endl;
                      cout << printName << " rolled " << dice1 << " and " << dice2 << endl;
                      cout << printName<<" did not roll doubles"<< endl;
                      input=selection2(rurc);
                  }
                  
                    if(input == "rimcup"){
                        playerList[playerID]->changeRurc(-1);
                        playerList[playerID]->rurcCount-=1;
                         playerList[playerID]->setDCStatus(-1);
                        cout << "-------INFORMATION-------" << endl;
                        cout << printName<<" used one Roll Up the Rim cup" << endl;
                        cout << "-------INFORMATION-------" << endl;
                        playerList[playerID]->changePosition(dice1+dice2);
                        draw();
                    }
                    if(input == "pay"){
                        if (give500 == true) {
                            playerList[0]->setSavings(0);
                            give500 = false;
                        }
                        playerList[0]->changeSavings(50);
                        playerList[playerID]->pay(50,0);
                        playerList[playerID]->setDCStatus(-1);
                        cout << "-------INFORMATION-------" << endl;
                        playerList[playerID]->changePosition(dice1+dice2);
                        draw();
                    }
                }
            }
        } // end if dice
        else if(input == "rimcup"){
            playerList[playerID]->setDCStatus(-1);
            playerList[playerID]->changeRurc(-1);
            playerList[playerID]->rurcCount-=1;
            cout << "-------INFORMATION-------" << endl;
            cout << printName << " used one Roll Up the Rim cup."<<printName<<" can leave DC Tims Line in next turn" << endl;
        }
        else{
            if (give500 == true) {
                playerList[0]->setSavings(0);
                give500 = false;
            }
            playerList[0]->changeSavings(50);
            int saving = playerList[playerID]->getSavings();
            if(saving < 50) {
                cout << "-------WARNING-------" << endl;
                cout << "You lack $" << 50-saving << " to pay to BANK" << endl;
                cout << "---------------------" << endl << endl;
                dcTimsLine(playerID,decision);
            }else{
                playerList[playerID]->changeSavings(-50);
                playerList[playerID]->setDCStatus(-1);
                cout << printName<<" can leave DC Tims Line in next turn" << endl;
            }
        }
    }
}

// function for goToTims

void Board::goToTims(int playerID,string decision){
    string printName = playerList[playerID]->getPiece() + " (" + playerList[playerID]->getName() + ")";
    cout << "-------INFORMATION-------" << endl;
    cout << printName << " will be sent to DC Tims Line Square" << endl;
    playerList[playerID]->setDCStatus(0);
    dcTimsLine(playerID,decision);
}


// function for gooseNesting

void Board::gooseNesting(int playerID){
    string printName = playerList[playerID]->getPiece() + " (" + playerList[playerID]->getName() + ")";
    cout << "-------INFORMATION-------" << endl;
    cout << printName << " will be attacked by a flock of nesting geese but get a bunch of money" << endl;
    if(give500 == true){
        playerList[playerID]->changeSavings(500);
        cout << "by being attacked by the nesting geese" << endl;
    }
    else{
        int centreMoney = playerList[0]->getSavings();
        playerList[playerID]->changeSavings(centreMoney);
        cout << "by being attacked by the nesting geese" << endl;
    }
    cout << "-------------------------" << endl << endl;
    playerList[0]->setSavings(0);
}


// function for Tuition

void Board::payTuition(int playerID,string decision) {
    if (give500 == true) {
        playerList[0]->setSavings(0);
        give500 = false;
    }
    string input;
    int totalWorth = playerList[playerID]->getSavings();
    for (int i=0; i<40; i++) {
        if (isOwnable[i] && buildingList[i]->getOwner() == playerList[playerID]->getPiece()) {
            totalWorth += buildingList[i]->getPurchaseCost();
            if (isAcademic[i]) {
                int improvement = buildingList[i]->getImprovement();
                totalWorth += buildingList[i]->getImprovementCost()*improvement;
            }
        }
    }
    while (1) {
        if(decision == ""){
            cout << "-------SELECTION-------" << endl;
            cout << "You must pay tuition to the BANK" << endl;
            cout << "You can choose to pay $300 directly" << endl;
            cout << "Or you may pay the 10% of your total worth: $";
            int tw = 0.1*totalWorth;
            cout << tw << endl;
            cout << "Please type 'amount' or 'percentage' ";
            cout << "(other input will not be recognized): ";
            cin >> input;
        }else{
            input = decision;
        }
        if (!cin) {
            cin.clear();
            cin.ignore();
            continue;
        } else if (input == "amount") {
            break;
        } else if (input == "percentage") {
            break;
        } else { continue; }
    }
    if(input == "amount") {
        playerList[playerID]->pay(300,0);
    }else{
        playerList[playerID]->pay(0.1*totalWorth,0);
    }
}

// function for CoopFee

void Board::payCoopFee(int playerID){
    string printName = playerList[playerID]->getPiece() + " (" + playerList[playerID]->getName() + ")";
    if (give500 == true) {
        playerList[0]->setSavings(0);
        give500 = false;
    }
    cout << "-------INFORMATION-------" << endl;
    cout << printName << " must pay coop fee to the BANK" << endl;
    playerList[playerID]->pay(150,0);
}


// function for getting a random probability
double giveProb(){
    static bool called = false;
    if(!called) {
        srand(time(NULL));
        called = true;
    }
    double prob = (double)rand() / (double)RAND_MAX;
    return prob;
}


// function for SLC
void Board::atSLC(int playerID){
    string printName = playerList[playerID]->getPiece() + " (" + playerList[playerID]->getName() + ")";
    double prob = giveProb();
    if(playerList[playerID]->rurcCount == 4) { prob = 0.5; }
    if(prob < 0.01){
        playerList[playerID]->rurcCount+=1;
        playerList[playerID]->changeRurc(1);
        cout << printName << " got one Roll up the Rim cup" << endl;
    }else{
        int move;
        prob = giveProb();
        if (prob < 1.0/24.0) {   // 1/24
            move = 0;
            cout << "-------INFORMATION-------" << endl;
            cout << printName << " is moved to Collect OSAP" << endl;
            playerList[playerID]->setPosition(0);
            collectOSAP(playerID);
        }
        else if (prob < 1.0/12.0) {    // 1/24
            move = 0;
            playerList[playerID]->setPosition(30);
        }
        else if (prob < 5.0/24.0) move = -3;   // 1/8
        else if (prob < 1.0/3.0)  move = 1;    // 1/8
        else if (prob < 1.0/2.0)  move = -2;   // 1/6
        else if (prob < 2.0/3.0)  move = -1;   // 1/6
        else if (prob < 5.0/6.0)  move = 2;    // 1/6
        else  move = 3;     // 1/6
        if(move!=0) {
            cout << "-------INFORMATION-------" << endl;
            playerList[playerID]->changePosition(move);
        }
    }
    draw();
}


// function for Needles Hall

void Board::atNeedlesHall(int playerID){
    string printName = playerList[playerID]->getPiece() + " (" + playerList[playerID]->getName() + ")";
    double prob = giveProb();
    if(playerList[playerID]->rurcCount == 4) { prob = 0.5; }
    if(prob < 0.01){
        playerList[playerID]->rurcCount+=1;
        playerList[playerID]->changeRurc(1);
        cout << printName << " got one Roll up the Rim cup" << endl;
    }else {
        prob = giveProb();
        if(prob < 1.0/18.0) { //  1/18
            playerList[playerID]->pay(200,0);
        }
        else if(prob < 1.0/9.0){ // 1/18
            cout << "-------INFORMATION-------" << endl;
            playerList[playerID]->changeSavings(200);
            cout << "since landing on Needles Hall" << endl;
            cout << "-------------------------" << endl << endl;
        }
        else if(prob < 2.0/9.0){ // 1/9
            playerList[playerID]->pay(100,0);
        }
        else if(prob < 1.0/3.0){ // 1/9
            cout << "-------INFORMATION-------" << endl;
            playerList[playerID]->changeSavings(100);
            cout << "since landing on Needles Hall" << endl;
            cout << "-------------------------" << endl << endl;
        }
        else if(prob < 1.0/2.0){ // 1/6
            playerList[playerID]->pay(50,0);
        }
        else if(prob < 2.0/3.0){ // 1/6
            cout << "-------INFORMATION-------" << endl;
            playerList[playerID]->changeSavings(50);
            cout << "since landing on Needles Hall" << endl;
            cout << "-------------------------" << endl << endl;
        }
        else{  // 1/3
            cout << "-------INFORMATION-------" << endl;
            playerList[playerID]->changeSavings(25);
            cout << "since landing on Needles Hall" << endl;
            cout << "-------------------------" << endl << endl;
        }
    }
}






