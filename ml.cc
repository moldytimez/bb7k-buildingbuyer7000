//
//  ml.cpp
//  bb7k
//
//  Created by jessica on 14/11/23.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "ml.h"
#include "arts1.h"
#include <iostream>
#include <map>
#include <string>
using namespace std;

ML::ML():Arts1(50,60,"ML"){
    tuitionList[0] = 4;
    tuitionList[1] = 20;
    tuitionList[2] = 60;
    tuitionList[3] = 180;
    tuitionList[4] = 320;
    tuitionList[5] = 450;
}


int ML::getPurchaseCost(){
    return purchaseCost;
}

int ML::getImprovementCost(){
    return improvementCost;
}

void ML::changeImprovement(string act){
    if(act == "in") improvement++;
    if(act == "de") improvement-=1;
    improvementList["ML"] = improvement;
}


int ML::getImprovement(){
    return improvement;
}

bool ML::checkMonopoly(bool whetherPrint){
    bool isMonopoly = false;
    if(ownerList["AL"] == owner) isMonopoly = true;
    else {
        cout << "You do not own the Monopoly" << endl;
        cout << "The property required to be owned is AL (owner: " << ownerList["AL"]<< ")" << endl;
    }
    return isMonopoly;
}



bool ML::checkMonopoly(){
    bool isMonopoly = false;
    if(ownerList["AL"] == owner) isMonopoly = true;
    return isMonopoly;
}


int ML::getFees(){
    int tuition = 0;
    if(checkMonopoly() && improvement == 0 && improvementList["AL"] == 0) tuition = tuitionList[improvement]*2;
    else tuition = tuitionList[improvement];
    return tuition;
}

int ML::getFees(string ownerPiece){return 0;}

string ML::getName(){
    return name;
}

string ML::getOwner(){
    return owner;
}

void ML::setOwner(string name){
    owner = name;
    ownerList["ML"] = name;
}


bool ML::getMortgage(){
    return isMortgage;
}

void ML::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}

ML::~ML(){}
