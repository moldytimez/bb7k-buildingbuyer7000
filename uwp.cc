//
//  uwp.cpp
//  bb7k
//
//  Created by jessica on 14/11/20.
//  Copyright (c) 2014年 oo. All rights reserved.
//

#include "uwp.h"
#include <iostream>
#include <string>
#include "residence.h"
#include "building.h"
using namespace std;


UWP::UWP():Residence("UWP"){}

string UWP::getOwner(){
    return owner;
}

void UWP::setOwner(string ownerPiece){
    resCount[owner]--;
    owner = ownerPiece;
    resCount[owner]++;
}

int UWP::getFees(){
    return rent;
}

int UWP::getFees(string ownerPiece){
    cout << "-------INFORMATION-------" << endl;
    cout << "Fees to pay depend on number of residences the owner owns" << endl;
    cout << "The owner owns " << resCount[ownerPiece] << " residence(s)" << endl;
    rent = resCount[ownerPiece]*25;
    return rent;
}


int UWP::getPurchaseCost(){
    return purchaseCost;
}


string UWP::getName(){
    return name;
}


bool UWP::getMortgage(){
    return isMortgage;
}

void UWP::setMortgage(bool whetherMortgage){
    isMortgage = whetherMortgage;
}


int UWP::getImprovementCost(){return 0;};
void UWP::changeImprovement(string act){}
int UWP::getImprovement(){return 0;}
bool UWP::checkMonopoly(bool whetherPrint){ return false;}
bool UWP::checkMonopoly(){return false;}
UWP::~UWP(){}
