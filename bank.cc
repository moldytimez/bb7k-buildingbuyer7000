#include "player.h"
#include "bank.h"
#include <string>
#include <iostream>
using namespace std;

Bank::Bank(Building **buildingList)
  :Player(0,"BANK","BANK",500,0,0,buildingList) {}
Bank::~Bank() {}

int Bank::getID() { return playerID; }
string Bank::getName() { return name; }
string Bank::getPiece() { return piece; }
int Bank::getSavings() { return savings; }
void Bank::setSavings(int s) { savings = s; }
int Bank::getPosition() { return 0; }
void Bank::setPosition(int buildingID) {}
void Bank::changePosition(int change){}
int Bank::getDCStatus() { return 0; }
void Bank::setDCStatus(int value) {}
int Bank::getRurc() { return 0; }
void Bank::changeRurc(int change) {}
void Bank::changeSavings(int change) {
    savings += change;
    cout << piece << " (" << name << ") ";
    if (change > 0) { cout << "got $" << change << " "; } }
void Bank::pay(int amount,int ownerID) {}
void Bank::buy(int buildingID) {}
void Bank::changeImprovement(int buildingID,char action) {}
void Bank::changeMortgage(int buildingID,char action) {}
void Bank::giveAsset() {}
void Bank::bankruptProcess(int amount,int ownerID) {}
bool Bank::getBankruptcy(){ return false; }
void Bank::setBankruptcy(bool a) {}
void Bank::strategy(string &decision) {}
