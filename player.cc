#include "player.h"
#include <map>
#include <string>
using namespace std;

// initialize static variable
int Player::rurcCount = 0;
Player *Player::playerList[7] = {0};
map<string,int> Player::IDList;
map<string,string> Player::nameList;
bool *Player::isOwnable = NULL;
bool *Player::isAcademic = NULL;

Player::Player(int playerID,string name,string piece,int savings,int position,int rurc,Building **buildingList):playerID(playerID),name(name),piece(piece),savings(savings),position(position),rurc(rurc),isBankrupt(false),dcStatus(-1),buildingList(buildingList) {
    for (int i=0; i<40; i++) { rejected[i] = ""; }
}

Player::~Player() {}
